// name    : setting_page.js
// SPDX-FileCopyrightText: 2017-2021 Simon Descarpentres <simon /\ acoeuro [] com>
// SPDX-License-Identifier: GPL-3.0-only
//
/* globals browser List Choices */
import * as µ from './utils.js'
import * as mµ from './mp_utils.js'
import * as g from './gettext_html_auto.js/gettext_html_auto.js'

async function setChoiceTz(mp_i18n) {
	var timezone = await fetch('json/zone.tab.json')
	//	https://stackoverflow.com/questions/39263321/javascript-get-html-timezone-dropdown
	timezone = await timezone.json()
	timezone.sort()
	var element = document.getElementById('mp_tz')
	for(let tz of Object.entries(timezone)) {
		var option = document.createElement('option')
		option.text = tz[1]
		option.value = tz[1]
		element.add(option)
	}
	let nav_date = new Date()
	let nav_tz = nav_date.getTimezoneOffset()
	var default_option = document.createElement('option')
	default_option.text = mp_i18n.gettext('Your navigator timezone')
	default_option.value = nav_tz
	element.add(default_option, 0)
}

(async () => {
	'use strict'
	await mµ.set_theme()
	await g.xgettext_html()
	const userLang = await mµ.get_wanted_locale()
	const mp_i18n = await g.gettext_html_auto(userLang)
	await setChoiceTz(mp_i18n)
	const getting = browser.runtime.getBackgroundPage()
	var sp_req ={
		autoSearchList: new List('automatic_search', {
			item: 'sch_sea_item',
			valueNames: ['sch_sea_query', 'sch_sea_nb','sch_sea_last_run',
				{ name: 'sch_sea_url', attr: 'value' },
				{ name: 'sch_sea_id', attr: 'id' },
				{ name: 'input_date', attr: 'value' },
				{ name: 'input_HHMM', attr: 'value' },
				{ name: 'min_date', attr: 'min' },
				{ name: 'title_1', attr: 'title'},
				{ name: 'title_2', attr: 'title'}
			]
		})
	}
	var select_opt = {
		resetScrollPosition: false,
		duplicateItemsAllowed: false,
		searchResultLimit: 8,
		shouldSort : false,
		searchFields: ['label']
	}
	//
	// general settings
	//
	var lang_select = document.getElementById('mp_lang')
	lang_select.value =  await mµ.get_stored_locale()
	lang_select.addEventListener('change', () => {
		browser.storage.sync.set({locale: µ.get_select_value(lang_select)})
		//localStorage.setItem('locale', µ.get_select_value(lang_select))
		location.reload()
	})
	var tz_select = document.getElementById('mp_tz')
	var choice_tz = new Choices(tz_select,Object.assign(select_opt))
	var tz = await mµ.get_stored_tz()
	choice_tz.setChoiceByValue(tz)
	tz_select.addEventListener('change', async() => {
		var tz_value = await µ.get_select_value(tz_select)
		browser.storage.sync.set({tz: tz_value})
		location.reload()
	})
	var bg_select = document.getElementById('dark_background')
	bg_select.value = await mµ.get_stored_theme()
	bg_select.addEventListener('change', () => {
		browser.storage.sync.set({dark_background: µ.get_select_value(bg_select)})
		//localStorage.setItem('dark_background', µ.get_select_value(bg_select))
		location.reload()
	})
	var live_search_reload = document.getElementById('live_search_reload')
	live_search_reload.checked = await mµ.get_stored_live_search_reload()
	live_search_reload.addEventListener('change', () => {
		browser.storage.sync.set({live_search_reload: live_search_reload.checked && '1' || ''})
		//localStorage.setItem('live_search_reload', live_search_reload.checked && '1' || '')
	})
	var sentence_search = document.getElementById('sentence_search')
	sentence_search.checked = await mµ.get_stored_sentence_search()
	sentence_search.addEventListener('change', () => {
		browser.storage.sync.set({sentence_search: sentence_search.checked && '1' || ''})
		//localStorage.setItem('sentence_search', sentence_search.checked && '1' || '')
	})
	var max_res_by_src = document.getElementById('max_res_by_src')
	max_res_by_src.value = await mµ.get_stored_max_res_by_src()
	max_res_by_src.addEventListener('change', () => {
		browser.storage.sync.set({max_res_by_src: max_res_by_src.value})
		//localStorage.setItem('max_res_by_src', max_res_by_src.value)
	})
	var undup_results = document.getElementById('undup_results')
	undup_results.checked = await mµ.get_stored_undup_results()
	undup_results.addEventListener('change', () => {
		browser.storage.sync.set({undup_results: undup_results.checked && '1' || ''})
	//localStorage.setItem('undup_results', undup_results.checked && '1' || '')
	})
	var load_photos = document.getElementById('load_photos')
	load_photos.checked = await mµ.get_stored_load_photos()
	load_photos.addEventListener('change', () => {
		browser.storage.sync.set({load_photos: load_photos.checked && '1' || ''})
	//localStorage.setItem('load_photos', load_photos.checked && '1' || '')
	})
	var headline_loading = document.getElementById('headline_loading')
	headline_loading.checked = await mµ.get_stored_headline_loading()
	headline_loading.addEventListener('change', () => {
		browser.storage.sync.set({headline_loading: headline_loading.checked && '1' || ''})
	//localStorage.setItem('headline_loading', headline_loading.checked && '1' || '')
	})
	var live_headline_reload = document.getElementById('live_headline_reload')
	live_headline_reload.checked = await mµ.get_stored_live_headline_reload()
	live_headline_reload.addEventListener('change', () => {
		browser.storage.sync.set({live_headline_reload: live_headline_reload.checked && '1' || ''})
	//localStorage.setItem('live_headline_reload', live_headline_reload.checked && '1' || '')
	})
	var max_headline_loading = document.getElementById('max_headline_loading')
	max_headline_loading.value = await mµ.get_stored_max_headline_loading()
	max_headline_loading.addEventListener('change', () => {
		browser.storage.sync.set({max_headline_loading: max_headline_loading.value})
	//localStorage.setItem('max_headline_loading', max_headline_loading.value)
	})
	var headline_page_size = document.getElementById('headline_page_size')
	headline_page_size.value = await mµ.get_stored_headline_page_size()
	headline_page_size.addEventListener('change', () => {
		browser.storage.sync.set({headline_page_size: headline_page_size.value})
	//localStorage.setItem('headline_page_size', headline_page_size.value)
	})
	var keep_host_perm = document.getElementById('keep_host_perm')
	keep_host_perm.checked = await mµ.get_stored_keep_host_perm()
	keep_host_perm.addEventListener('change', () => {
		browser.storage.sync.set({keep_host_perm: keep_host_perm.checked && '1' || ''})
	//localStorage.setItem('keep_host_perm', keep_host_perm.checked && '1' || '')
	})
	var provided_sources_json = await fetch('json/sources.json')
	provided_sources_json = await provided_sources_json.json()
	document.getElementById('request_host_perm').addEventListener('click', () => {
		mµ.request_sources_perm(provided_sources_json)
	})
	document.getElementById('drop_host_perm').addEventListener('click', mµ.drop_host_perm)
	//
	// auto search
	//
	var last_run, next_run, run_freq, sch_sea_date, sch_sea_date_clone, nav_to_tz, browser_str
	function background_timezoned_date(a) {
		return a.background_timezoned_date(sch_sea_date_clone, tz, nav_to_tz)
	}
	function background_maj_date(a) {
		set_timezone(next_run)
		return a.sch_sea_maj_date(next_run, run_freq)
	}
	async function set_timezone(sch_sea_date, bool=true) {
		tz = await mµ.get_stored_tz()
		nav_to_tz = bool
		sch_sea_date_clone = sch_sea_date
		if (sch_sea_date && sch_sea_date != 0) {
			sch_sea_date = await getting.then(background_timezoned_date)
		}
		return sch_sea_date
	}
	function background_init_table_sch_s(a) {
		 return a.init_table_sch_s(browser_str)
	}
	async function schedule_search(elt) {
		browser_str = elt
		if(Array.isArray(elt)) {var table_sch_s = elt}
		else {var table_sch_s = await getting.then(background_init_table_sch_s)}
		suite_schedule_search(table_sch_s)
	}
	async function suite_schedule_search(table_sch_s) {
		var table_sch_s_length = table_sch_s.length
		if (table_sch_s_length) {
			document.getElementById('automatic_search').style.display = 'block'
			document.querySelector('#automatic_search .list').style.visibility = 'visible'
			sp_req.autoSearchList.clear()  // to remove the template
		}
		for(let i=table_sch_s_length;i--;) {
			var intlNum = Intl.NumberFormat('fr', {minimumIntegerDigits: 2, useGrouping: 0})
			var local_url = new URL(window.location).origin
			var new_url = new URL(local_url + table_sch_s[i])
			let temp = await mµ.set_text_params(new_url, mp_i18n)
			var list_p = temp['list_p']
			var params = temp['params']
			last_run = new_url.searchParams.get('last_run')
			last_run = last_run==0 ? 0 : new Date(last_run)
			if(last_run != 0) {
				last_run = await set_timezone(last_run)
				var last_run_hm = intlNum.format(
					last_run.getHours()) +':'+intlNum.format(last_run.getMinutes())
				var last_run_ymd = last_run.getFullYear() +'-'+ intlNum.format(
					last_run.getMonth()+1) +'-'+ intlNum.format(last_run.getDate())
			}
			run_freq = new_url.searchParams.get('run_freq')
			next_run = new_url.searchParams.get('next_run')     // local background_maj_date needs
			next_run = await getting.then(background_maj_date)  // a global next_run
			next_run = new Date(next_run).toUTCString()
			let id = `sch_sea__${i}`
			new_url = new URL(local_url + '/index.html' + new_url.search)
			new_url.searchParams.set('next_run',next_run)
			new_url.searchParams.set('id_sch_s',id)
			table_sch_s[i] = new_url.search
			sch_sea_date = new Date()
			sch_sea_date = await set_timezone(sch_sea_date)
			sch_sea_date = sch_sea_date.toISOString()
			var next_tz = await set_timezone(next_run)
			var next_run_hm = intlNum.format(
				next_tz.getHours()) + ':' + intlNum.format(next_tz.getMinutes())
			var next_run_ymd = next_tz.getFullYear() +'-'+ intlNum.format(
				next_tz.getMonth()+1) + '-'+ intlNum.format(next_tz.getDate())
			sp_req.autoSearchList.add({
				sch_sea_nb: i,
				sch_sea_url: new_url,
				sch_sea_query: list_p['q'],
				title_1 : params,
				title_2 : params,
				sch_sea_last_run: last_run==0 ? 0 : last_run_ymd+' '+last_run_hm,
				sch_sea_id: id,
				input_date: next_run_ymd,
				input_HHMM: next_run_hm,
				min_date: sch_sea_date.slice(0,10)
			})
			document.getElementById(id).parentElement.querySelector('.frq').value = run_freq
		}
		add_all_events(table_sch_s)
		sp_req.autoSearchList.sort('sch_sea_id', { order: "asc" })
		browser.alarms.clearAll()
		tab = table_sch_s
		await getting.then(b_create_alarm)
	}
	var sch_s
	function b_launch_sch_s(elt) {
		elt.launch_sch_s(sch_s)
	}
	function show_sch_s_save_btn_cb (evt) {
		let local_save_btn = evt.target.parentElement.querySelector('.save_date')
		local_save_btn.style.visibility = 'visible'
	}
	function add_all_events(table_sch_s) {
		for(let i=table_sch_s.length; i--;) {
			let local_url = new URL(window.location).origin
			let new_url = new URL(local_url + table_sch_s[i])
			let id = `sch_sea__${i}`
			let elt = document.getElementById(id).parentElement
			elt.querySelector('.frq').addEventListener('change', () => {
				save_as_date(id, table_sch_s)})
			var save_btn = elt.querySelector('.save_date')
			elt.querySelector('.input_date').addEventListener('change', show_sch_s_save_btn_cb)
			elt.querySelector('.input_HHMM').addEventListener('change', show_sch_s_save_btn_cb)
			save_btn.addEventListener('click', (evt) => {
				save_as_date(id, table_sch_s)
				let local_save_btn = evt.target.parentElement.querySelector('.save_date')
				local_save_btn.style.visibility = 'hidden'
			})
			elt.querySelector('.suppr_sch_sea').addEventListener('click', () => {
				del_sch_sea(elt, table_sch_s)})
			elt.querySelector('.sch_sea_url').addEventListener('click', (evt) => {
				browser.tabs.create({ 'url': evt.target.value })
			})
			elt.querySelector('.play').addEventListener('click', () => {
				sch_s = table_sch_s[i]
				getting.then(b_launch_sch_s)
			})
			elt.querySelector('.duplicate').addEventListener('click', () => {
				new_url.searchParams.delete('id_sch_s')
				sp_req.autoSearchList.clear()
				add_elt(new_url, table_sch_s)
			})
		}
	}
	async function add_sch_sea(elt) {
		browser_str = elt
		var table_sch_s = await getting.then(background_init_table_sch_s)
		var new_sch_sea = elt.new_sch_sea
		add_elt(new_sch_sea, table_sch_s)
	}
	var m_id_sch_s, m_value_sch_s //value of modified schedule search  use in background.js
	async function add_elt(new_sch_sea, table_sch_s) {
		sch_sea_date = new Date()
		var next_run = new Date(
			sch_sea_date.getFullYear(), sch_sea_date.getMonth(), sch_sea_date.getDate()+1, 0, 0
		).toUTCString()
		if(new_sch_sea) {
			let new_url = new URL(new_sch_sea)
			let id = new_url.searchParams.get('id_sch_s')
			if(!new_url.searchParams.get('next_run')) {new_url.searchParams.set('next_run',next_run)}
			if(!new_url.searchParams.get('last_run')) {new_url.searchParams.set('last_run','0')}
			if(!new_url.searchParams.get('run_freq')) {
				new_url.searchParams.set('run_freq','sch_s_stop')
			}
			if(id) {
				id = id.split('__')[1]
				table_sch_s.splice(id,1,new_url.search)
			}
			else {
				id = table_sch_s.length
				new_url.searchParams.set('id_sch_s',`sch_sea__${id}`)
				table_sch_s.push(new_url.search)
			}
			m_id_sch_s = `sch_sea__${id}`
			m_value_sch_s = table_sch_s[id]
			await getting.then((elt)=>{elt.modif_sch_s_storage(m_id_sch_s, m_value_sch_s)})
			browser.storage.sync.remove('new_sch_sea')
			schedule_search(table_sch_s)
		}
	}
	async function del_sch_sea(html_elt, table_sch_s){
		html_elt = html_elt.querySelector('td')
		let del_id = html_elt.id.split('__')[1]
		let del_url = table_sch_s[del_id]
		sp_req.autoSearchList.clear()
		if(del_id==table_sch_s.length-1) {
			browser.storage.sync.remove(`sch_sea__${del_id}`)//if last element delete it in browser storage
		} else {
			for(let i=del_id;i<table_sch_s.length-1;i++) {// for all element after
				m_id_sch_s = `sch_sea__${i}`
				m_value_sch_s = table_sch_s[parseInt(i)+1]
				m_value_sch_s = m_value_sch_s.replace(/sch_sea__[0-9]*/,`sch_sea__${i}`)//decremente value of id
				await getting.then((elt)=>{elt.modif_sch_s_storage(m_id_sch_s, m_value_sch_s)})
				browser.storage.sync.remove(`sch_sea__${parseInt(i)+1}`)//remove old element with bad id
			}
		}
		browser.storage.sync.get().then(getElt)
		browser.alarms.clear(del_url)
	}
	var tab
	function b_create_alarm(elt){
		elt.create_alarm(tab)
	}
	async function save_as_date(id, table_sch_s) {
		let new_id = id.split('__')[1]
		let local_url = new URL(window.location).origin
		let new_url = new URL(local_url + table_sch_s[new_id])
		let elt = document.getElementById(id).parentElement
		let new_date = elt.querySelector('.input_date').value
		let new_hours = elt.querySelector('.input_HHMM').value
		let new_run_freq = elt.querySelector('.frq').value
		new_date = new_date.split('-')
		let year = new_date[0]
		let month = new_date[1]
		let day = new_date[2]
		new_hours = new_hours.split(':')
		let hours = new_hours[0]
		let minute = new_hours[1]
		var next_run = new_url.searchParams.get('next_run')
		next_run = new Date(year, month-1, day, hours, minute, 0)
		next_run = await set_timezone(new Date(next_run),false)
		next_run = next_run.toUTCString()
		if(!elt.querySelector('.input_date:invalid')) {
			new_url.searchParams.set('next_run',next_run)
			new_url.searchParams.set('run_freq',new_run_freq)
			elt.querySelector('.sch_sea_url').value = local_url + '/index.html'+new_url.search
			table_sch_s[new_id] = new_url.search
			browser.alarms.clearAll()
			m_id_sch_s = id
			m_value_sch_s = table_sch_s[new_id]
			await getting.then((elt)=>{elt.modif_sch_s_storage(m_id_sch_s, m_value_sch_s)})
			tab = table_sch_s
			await getting.then(b_create_alarm)
		}
	}
	await browser.storage.sync.get().then(getElt)
	function getElt(elt) {
		if(elt.new_sch_sea) {add_sch_sea(elt)}
		else {schedule_search(elt)}
	}
})()
