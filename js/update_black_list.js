// name   : update_locales.js
// SPDX-FileCopyrightText: 2019 Simon Descarpentres <simon /\ acoeuro [] com>
// SPDX-License-Identifier: GPL-3.0-only

/* globals require */

'use strict'
const is_array = v => typeof(v) == 'object' && typeof(v.length) != 'undefined'
const fs = require('fs')
const prefix = 'html_locales/'
const bl = 'black_list.json'
const bl_json = JSON.parse(fs.readFileSync(`${prefix}${bl}`, 'utf8'))
const src_json = JSON.parse(fs.readFileSync(`json/sources.json`, 'utf8'))
const src_list = Object.keys(src_json)
const intl_lang_name = (lg) => new Intl.DisplayNames([lg], {type: 'language'}).of(lg)

var tags
for (let k of src_list) {
	tags = src_json[k].tags
	try {
		/*
		if (typeof(tags.name) == 'undefined')
			console.log(k, 'undefined tags.name')
		if (typeof(tags.country) == 'undefined')
			console.log(k, 'undefined tags.country')
		if (typeof(tags.lang) == 'undefined')
			console.log(k, 'undefined tags.lang')
			*/
		if (typeof(tags.name) != 'undefined')
			bl_json[tags.name] = ''
		if (typeof(tags.country) != 'undefined')
			bl_json[tags.country] = ''
		if (typeof(tags.lang) != 'undefined') {
			bl_json[`[${tags.lang}]`] = ''
			bl_json[intl_lang_name(tags.lang)] = ''
		}
		if (is_array(tags.lang)) {
			for (let l of tags.lang)
				bl_json[l] = ''
		} else {
			if (typeof(tags.lang) != 'undefined')
				bl_json[tags.lang] = ''
		}
	} catch (exc) {
		console.log(exc)
	}
}
for (let i = src_list.length; i--;) {
	bl_json[` (${i})`] = ''
}
fs.writeFileSync(`${prefix}${bl}`, JSON.stringify(bl_json, null, 2))
