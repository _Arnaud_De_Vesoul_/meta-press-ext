// name    : setting_page.js
// SPDX-FileCopyrightText: 2021 Simon Descarpentres <simon /\ acoeuro [] com>
// SPDX-License-Identifier: GPL-3.0-only
//
/* globals browser CodeMirror */
import * as mµ from './mp_utils.js'
import * as g from './gettext_html_auto.js/gettext_html_auto.js'

(async () => {
	'use strict'
	await mµ.set_theme()
	const userLang = await mµ.get_wanted_locale()
	await g.xgettext_html()
	/*const mp_i18n = */await g.gettext_html_auto(userLang)
	//
	// custom_src
	//
	var provided_sources_text = await fetch('json/sources.json')
	provided_sources_text = await provided_sources_text.text()
	var custom_src_codemirror
	document.getElementById('provided_sources').textContent = provided_sources_text
	/* var provided_src_codemirror = */ CodeMirror.fromTextArea(
		document.getElementById('provided_sources'), {
			mode: 'application/json',
			// mode: {name:'javascript', json:true},
			indentWithTabs: true,
			screenReaderLabel: 'CodeMirror',
			readOnly: 'nocursor'
		}
	)
	browser.storage.sync.get('custom_src').then(
		load_custom_src, err => {console.error(`Loading custom_src: ${err}`)}
	)
	function load_custom_src(stored_data){
		if (typeof(stored_data) == 'object' && typeof(stored_data.custom_src) == 'string' &&
				stored_data.custom_src && stored_data.custom_src != '{}') {
			document.getElementById('custom_sources').textContent = stored_data.custom_src
			// document.getElementById('reload_hint').style.display = 'inline'
		} else {
			document.getElementById('custom_sources').textContent =
				document.getElementById('default_custom_sources').textContent
		}
		custom_src_codemirror = CodeMirror.fromTextArea(
			document.getElementById('custom_sources'), {
				mode: 'application/json',
				screenReaderLabel: 'CodeMirror',
				indentWithTabs: true,
				extraKeys: {Tab: false} // avoids keyboard trap preserving 'tab-key' navigation
			}
		)
		const STATUS_CURRENT_LINE = document.getElementById('ln_nb')
		const STATUS_CURRENT_COL = document.getElementById('col_nb')
		custom_src_codemirror.on('cursorActivity', () => {
			const cursor = custom_src_codemirror.getCursor()
			STATUS_CURRENT_LINE.textContent = cursor.line + 1
			STATUS_CURRENT_COL.textContent = cursor.ch + 1
		})
	}
	document.getElementById('save_custom_sources').addEventListener('click', () => {
		custom_src_codemirror.save()
		const custom_src_value = document.getElementById('custom_sources').value
		if (custom_src_value != '')
			try {
				JSON.parse(custom_src_value)
			} catch (exc) {
				alert(`Error parsing user custom source (${exc}).`)
				return
			}
		browser.storage.sync.set({custom_src: custom_src_value})
		document.getElementById('reload_hint').style.display = 'inline'
		document.getElementById('reload_hint').style['font-weight'] = 'bold'
	})
	document.getElementById('reset_custom_sources').addEventListener('click', () => {
		browser.storage.sync.set({custom_src: ''})
	})
})()
