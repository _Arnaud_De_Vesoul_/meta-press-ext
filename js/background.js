// name	  : background.js
// SPDX-FileCopyrightText: 2017-2021 Simon Descarpentres <simon /\ acoeuro [] com>
// SPDX-License-Identifier: GPL-3.0-only
// date   : 2017-06, 2020-10
/* globals browser */
/* Open a new tab, and load "index.html" in it. */


//browser.storage.sync.clear()  // in case of schedule search panic, clear browser storage

function open_meta_press_es() {
	console.log('injecting meta-press.es')
	browser.tabs.create({
		'url': '/index.html'
	})
}

async function announce_updates (stored_version) {
	if (typeof(stored_version['version']) == 'undefined') {
		browser.tabs.create({'url': '/welcome.html'})
	}
	if (stored_version['version'] == '1.7.1') { /* to avoid scheduled search panic */
		browser.alarms.clearAll()
		browser.storage.sync.set({sch_sea: []})
	}
	let manifest = await fetch('manifest.json')
	manifest = await manifest.json()
	browser.storage.sync.set({version: manifest.version})
	var do_announce_updates = false
	if (do_announce_updates) {
		if (stored_version['version'] != manifest.version)
			browser.tabs.create({'url': 'https://www.meta-press.es/category/journal.html'})
	}
}

/* Schedule Search */
function sch_sea_maj_date(date, frq) {
	var cur_date = new Date()
	let d = date
	if(cur_date >= date && frq != 'sch_s_stop') {
		if(frq == 'sch_s_hourly') {
			d = cur_date
			d = new Date(d.setMinutes(date.getMinutes()))
			if(d <= cur_date) {
				d = new Date(d.setTime(d.getTime()+3600000))
			}
		}
		if(frq == 'sch_s_daily') {
			d = cur_date
			d = new Date(d.setMinutes(date.getMinutes()))
			d = new Date(d.setHours(date.getHours()))
			if(d <= cur_date) {
				d = new Date(d.setTime(d.getTime()+86400000))
			}
		}
		if(frq == 'sch_s_weekly') {
			d = cur_date
			d = new Date(d.setMinutes(date.getMinutes()))
			d = new Date(d.setHours(date.getHours()))
			let mod = (7 - cur_date.getDay() + date.getDay())%7
			d = new Date(d.setTime(d.getTime()+(86400000*mod)))
			if(d <= cur_date) {
				d = new Date(d.setTime(d.getTime()+(86400000*7)))
			}
		}
		if(frq == 'sch_s_monthly' || frq == 'sch_s_quaterly' || frq == 'sch_s_half-yearly')
		{
			d = new Date(d.setMonth(cur_date.getMonth()))
			d = new Date(d.setFullYear(cur_date.getFullYear()))
			if(frq == 'sch_s_monthly') {
				if(d < cur_date) { d = date.setMonth(date.getMonth() + 1)}
			}
			if(frq == 'sch_s_quaterly') {
				if(!(d.getMonth() - date.getMonth())%3) {
					let mod = (3 - (d.getMonth() - date.getMonth()))%3
					d = d.setMonth(d.getMonth() + mod)
				}
				if(d < cur_date) { d = date.setMonth(date.getMonth() + 3)}
			}
			if(frq == 'sch_s_half-yearly') {
				if(!(d.getMonth() - date.getMonth())%6) {
					let mod = (6 - (d.getMonth() - date.getMonth()))%6
					d = d.setMonth(d.getMonth() + mod)
				}
				if(d < cur_date) { d = date.setMonth(date.getMonth() + 6)}
			}
			if(d.getMonth() < cur_date.getMonth()) {
				d = new Date(d)
				d = d.setFullYear(cur_date.getFullYear()+1)
			}
		}
		if(frq == 'sch_s_annual') {
			d = date.setFullYear(cur_date.getFullYear())
			if(d < cur_date) { d = date.setFullYear(date.getFullYear() + 1)}
		}
	}
	return new Date(d)
}
function init_table_sch_s(browser_str) {
	var table_sch_s = []//init value of table_sch_s
	var keys_elt_browser_str = Object.keys(browser_str)//get all keys of object in browser storage
	var entries_elt_browser_str = Object.entries(browser_str)//get all entries in browser storage
	for(let i=0; i<keys_elt_browser_str.length;i++) {// for all keys
		var regex = /sch_sea__/
		if(keys_elt_browser_str[i].match(regex)) {//if keys match with 'sch_sea__'
			let id = keys_elt_browser_str[i].split('__')[1]
			table_sch_s[id] = entries_elt_browser_str[i]//add entries in table_sch_s
		}
	}
	return table_sch_s
}
function modif_sch_s_storage(id_sch_s, value_sch_s) {
	var modif_sch_s = {}
	modif_sch_s[id_sch_s] = value_sch_s
	browser.storage.sync.set(modif_sch_s)
}
var table_sch_s
browser.storage.sync.get().then(getElt)
function getElt(elt) {
	var stored_sch_sea = init_table_sch_s(elt)
	if(stored_sch_sea) {create_alarm(stored_sch_sea)}
}
async function launch_sch_s(sch_s) {
	sch_s = sch_s.name ? sch_s.name : sch_s
	let local_url = new URL(window.location).origin
	let new_url = new URL(local_url +'/index.html' + sch_s)
	let next_run = new_url.searchParams.get('next_run')
	let run_freq = new_url.searchParams.get('run_freq')
	let index = new_url.searchParams.get('id_sch_s')
	let last_run = new Date().toUTCString()
	next_run = await sch_sea_maj_date(new Date(next_run), run_freq)
	next_run = next_run.toUTCString()
	new_url.searchParams.set('last_run', last_run)
	new_url.searchParams.set('next_run', next_run)
	modif_sch_s_storage(index, new_url.search)
	new_url.searchParams.set('submit', '1')
	new_url.searchParams.set('last_res', 0)
	browser.tabs.create({
		'url': new_url.toString()
	})
	create_alarm([new_url.search])
}
function create_alarm(sch_s) {
	table_sch_s = sch_s
	for(let sch_sea of sch_s) {
		if(Array.isArray(sch_sea)){sch_sea=sch_sea[1]}
		let local_url = new URL(window.location).origin
		var new_url = new URL(local_url +'/index.html' + sch_sea)
		var next_run = new_url.searchParams.get('next_run')
		var run_freq = new_url.searchParams.get('run_freq')
		var next_run_date = new Date(next_run)
		if(run_freq != 'sch_s_stop') {
			next_run_date = next_run_date.getTime()
			browser.alarms.create(sch_sea, {when: next_run_date})
			browser.alarms.onAlarm.addListener(launch_sch_s)
		}
	}
	console_debug_alarms(bool_debug)
}
var bool_debug = false
async function console_debug_alarms(bool) {
	if(bool) {
		var alarms
		await browser.alarms.getAll().then(elt => {alarms = elt})
		console.log(alarms)
		for(let i=alarms.length;i--;) {
			let date = new Date(alarms[i].scheduledTime)
			console.log("L'alarme ",i," se lancera le ",date)
		}
	}
}
var intlNum = Intl.NumberFormat('fr', {minimumIntegerDigits: 4, useGrouping: 0})
function background_timezoned_date (dt_str, tz='UTC', nav_to_tz) {
	var dt = dt_str ? new Date(dt_str) : new Date()
	if (isNaN(dt)) throw new Error(`${dt_str} is an invalid Date`)
	if (tz == 'UTC' || tz == 'GMT') return dt
	let parsed_tz = parseInt(tz,10)
	if(isNaN(parsed_tz)) {
		var dt_orig = new Date(dt.getTime() - dt.getTimezoneOffset()*60*1000)
		var dt_UTC = new Date(dt.getTime() + dt.getTimezoneOffset()*60*1000)
		var dt_repr = dt_UTC.toLocaleTimeString('fr', {timeZoneName: 'short', timeZone: tz})
		var int_offset, date_tz
		if(nav_to_tz) {
			date_tz = dt_UTC
			int_offset = parseInt(
				-(dt_repr.split('UTC')[1].replace('−', '-').replace(':', '.') * 100))
		} else {
			date_tz = dt_orig
			int_offset = parseInt((dt_repr.split('UTC')[1].replace('−', '-').replace(':', '.') * 100))
		}
		var tz_offset = intlNum.format(int_offset)
		var tz_repr = int_offset >= 0 ? '+'+tz_offset : tz_offset
		return new Date(date_tz/*_UTC*/.toISOString().replace(/\.\d{3}Z/, tz_repr))
	}
	else { return new Date(dt) }
}
var provided_sources
async function load_prov_src(){
	provided_sources = await fetch('json/sources.json')
	provided_sources = await provided_sources.json()
}
function get_prv_src(src_name) {
	var prv = {}
	if(src_name == "") {
		for (let t of Object.keys(provided_sources)) {
			prv[t] = provided_sources[t]
		}
	} else {
		for (let t of Object.keys(provided_sources)) {
			if(provided_sources[t].tags.name == src_name) {
				prv[t] = provided_sources[t]
				let ext = prv[t].extends
				if(ext) {prv[ext] = provided_sources[ext]}
			}
		}
	}
	return prv
}

browser.browserAction.onClicked.addListener(open_meta_press_es)
/* Announces updates : opens the official link when new version is installed */
browser.storage.sync.get('version').then(announce_updates,
	() => {console.error('Could not get "version" from browser storage')}
)
load_prov_src()
