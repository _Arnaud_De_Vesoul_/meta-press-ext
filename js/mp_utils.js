// name		 : mp_utils.js
// SPDX-FileCopyrightText: 2017-2021 Simon Descarpentres <simon /\ acoeuro [] com>
// SPDX-License-Identifier: GPL-3.0-only
/* globals browser */
import * as µ from './utils.js'

export async function get_wanted_theme() {
	return await get_stored_theme() || (µ.isDarkMode() && 'dark' || 'light')
}
export async function set_theme() { if (await get_wanted_theme() == 'light') set_light_mode() }
export function set_light_mode() {
	var html = document.getElementsByTagName('html')[0]
	html.style.cssText += '--turquoise-background: var(--mp-turquoise)'
	html.style.cssText += '--background: var(--light-normal-background)'
	html.style.cssText += '--foreground: black'
	html.style.cssText += '--frame-background: var(--light-frame-background)'
	html.style.cssText += '--a-color: var(--mp-dark-turquoise)'
	html.style.cssText += '--mp-gray: var(--dark-gray)'
	var imgs = document.querySelectorAll('img[src$="_dark.svg"]')
	for (var img of imgs)
		img.src = img.src.replace('_dark', '')
}
export async function get_wanted_locale() {
	return await get_stored_locale() || navigator.language || navigator.userLanguage
}
export const get_stored_locale = async() => await µ.get_stored('locale', '')
export const get_stored_tz = async() => await µ.get_stored(
	'tz', (new Date().getTimezoneOffset()).toString())
export const get_stored_theme = async() => await µ.get_stored('dark_background', '')
export const get_stored_live_search_reload = async() => await µ.get_stored(
	'live_search_reload', '')
export const get_stored_sentence_search = async() => await µ.get_stored('sentence_search', '1')
export const get_stored_undup_results = async() => await µ.get_stored('undup_results', '1')
export const get_stored_load_photos = async() => await µ.get_stored('load_photos', '1')
export const get_stored_max_res_by_src = async() => await µ.get_stored('max_res_by_src', 20)
export const get_stored_headline_loading = async() => await µ.get_stored(
	'headline_loading', '')
export const get_stored_live_headline_reload = async() => await µ.get_stored(
	'live_headline_reload', '1')
export const get_stored_keep_host_perm = async() => await µ.get_stored('keep_host_perm', '1')
export const get_stored_max_headline_loading = async() => await µ.get_stored(
	'max_headline_loading', 30)
export const get_stored_headline_page_size = async() => await µ.get_stored(
	'headline_page_size', 5)
export function notifyUser(title, body) {
	// Let's check if the browser supports notifications
	if (!('Notification' in window)) console.warn('Browser don\'t support desktop notification')
	// Let's check whether notification permissions have already been granted
	else if (Notification.permission === 'granted') createNotification(title, body)
	// If it's okay let's create a notification
	else if (Notification.permission !== 'denied')
		// The Notification permission may only be requested from inside a short running
		// user-generated event handler.
		Notification.requestPermission().then(function (permission) {
			// If the user accepts, let's create a notification
			if (permission === 'granted') createNotification(title, body)
		})
	// At last, if the user has denied notifications, and you
	// want to be respectful there is no need to bother them any more.
}
function createNotification(title, body) {
	new Notification(title, {
		body: body,
		icon: 'img/favicon-metapress-v2.png',
		image: 'img/logo-metapress_sq.svg',
		badge: 'img/favicon-metapress-v2.png',
		actions: [{}]
	})
}
export async function drop_host_perm () {
	let perm = await browser.permissions.getAll()
	// console.log(`Host permissions to drop : ${perm.origins.join(', ')}`)
	// console.log(`API permissions to drop : ${perm.permissions.join(', ')}`)
	browser.permissions.remove({origins: perm.origins})
}
export async function check_host_perm(current_sources, sources_objs) {
	let bool_perm = true
	let perm = await browser.permissions.getAll()
	let current_host_perm  = get_current_source_headline_hosts(current_sources, sources_objs)
	for(let val of current_host_perm) {
		bool_perm = perm.origins.includes(val)
	}
	return bool_perm
}
export function get_current_source_search_hosts (current_source_selection, sources_objs) {
	let source_hosts = []
	for (let s of current_source_selection) {
		source_hosts.push(`${s}/*`)
		let s_surl = sources_objs[s].search_url
		if(s_surl && s_surl != "") {
			if (s != µ.domain_part(s_surl))	{
				source_hosts.push(`${µ.domain_part(sources_objs[s].search_url)}/*`)}
			if (sources_objs[s].redir_url)
				source_hosts.push(`${µ.domain_part(sources_objs[s].redir_url)}/*`)
		}
	}
	return source_hosts
}
export function get_current_source_headline_hosts (current_source_selection, sources_objs) {
	let source_hosts = []
	for (let s of current_source_selection) {
		let s_hurl = sources_objs[s].headline_url
		if(s_hurl && s_hurl != "" && s_hurl != "https://www.custom-source.eu"){
			source_hosts.push(`${µ.domain_part(s_hurl)}/*`) }
	}
	return source_hosts
}
export function request_sources_perm (sources) {
	let s_surl = get_current_source_search_hosts(Object.keys(sources), sources)
	let perm = {origins : new Set(s_surl)}
	let s_hurl = get_current_source_headline_hosts(Object.keys(sources), sources)
	for (let sh of s_hurl) {
		perm.origins.add(sh)
	}
	perm.origins = Array.from(perm.origins)
	//console.log(perm)
	browser.permissions.request(perm)
}
export function img_tag(src, alt, title) {
	return `<img src="${µ.encode_XML(src)}" alt="${alt}" title="${title}"/>`
}
export function extend_source(n, extended_src) {
	extended_src = JSON.parse(JSON.stringify(extended_src))
	let extended_tags = Object.assign(extended_src.tags || {}, n.tags)
	Object.assign(extended_src, n)
	n = extended_src
	n.tags = extended_tags
	return n
}
export async function set_text_params(url, mp_i18n) {
	var params = ''
	var list_p = {
		'q':``,
		'src_type':`${mp_i18n.gettext('Type:')} `,
		'lang':`${mp_i18n.gettext('Language:')} `,
		'res_type':`${mp_i18n.gettext('Result type:')} `,
		'themes':`${mp_i18n.gettext('Themes:')} `,
		'tech':`${mp_i18n.gettext('Technical criterion:')} `,
		'country':`${mp_i18n.gettext('Country:')} `,
		'scope':`${mp_i18n.gettext('Scope:')} `,
		'name':`${mp_i18n.gettext('Cheery-pick sources:')} `
	}
	for(let i of Object.keys(list_p)) {
		if(url.searchParams.get(i) && url.searchParams.get(i)!='') {
			if (i == 'q') {list_p[i] += url.searchParams.get(i)}
			else {list_p[i] += url.searchParams.getAll(i).join(', ')}
		} else {
			if (i == 'q') {list_p[i] = mp_i18n.gettext('No search terms')}
			else {list_p[i] = ''}
		}
		if(list_p[i]!='') {
			if(i != 'q') {params += list_p[i] + '\n'}
		}
	}
	return {list_p, params}
}
