// name		: meta-press.js
// SPDX-FileCopyrightText: 2017-2021 Simon Descarpentres <simon /\ acoeuro [] com>
// SPDX-License-Identifier: GPL-3.0-only
//
/* globals List, Choices, browser */
// domParser : Working Draft (Firefox 12)
// crypto : Firefox 26
// fetch : Firefox 39
// includes : Firefox 43
// permissions : Firefox 48
// storage : Firefox 48
// Intl.DisplayNames : unsupported yet (Chrome 81 - 2020-04-20)
// XPathEvaluator.evaluate
//
import * as µ from './utils.js'
import * as mµ from './mp_utils.js'
import * as g from './gettext_html_auto.js/gettext_html_auto.js'
import {month_nb} from './month_nb/month_nb.js'

var provided_sources
var mp_req

function make_mp_print (name,nat_obj) {
	return function () {
		log += `${name} : `
		nat_obj.apply(console, arguments)
		for (var i=0; i<arguments.length; i++)
			log += ' '+ String(arguments[i])
		log += '\n'
	}
}
var alert_console = false
if (alert_console) {
	var log = ''
	console.error = make_mp_print('Error', console.error)
	console.warning = make_mp_print('Warning', console.warning)
	console.log = make_mp_print('Log', console.log)
	console.info = make_mp_print('Info', console.info)
	let btn = document.getElementById('mp_dev')
	btn.style.display = 'block'
	btn.addEventListener('click', () => alert(log))
}

/* TEST SOURCES */
function sources_tested(src_tested_name, src_tested_query, local_url) {
	var elt = document.getElementById('mp_display_result_test')
	var pool = parseInt(elt.getAttribute('data-pool'))
	if(pool > 0) {
		elt.setAttribute('data-pool',pool-1)
		var new_url = local_url
		new_url.searchParams.set('q',src_tested_query)
		new_url.searchParams.set('name',src_tested_name)
		new_url.searchParams.set('submit',1)
		new_url.searchParams.delete('launch_test')
		let tested
		if(!document.getElementById(src_tested_name)) {
			tested = document.createElement('a')
			tested.id = src_tested_name
			tested.target = '_blank'
			elt.append(tested)
		} else {
			tested = document.getElementById(src_tested_name)
		}
		tested.title = ''
		tested.setAttribute('class', 'default')
		tested.textContent = 'O'
		new_url.searchParams.set('test_sources',1)
		window.open(new_url.toString(), '_blank')
	} else {
		setTimeout(function () {sources_tested(src_tested_name, src_tested_query, local_url)}, 200)
	}
}
async function retest_sources() {
	document.getElementById('mp_test_results').style = "display:none;"
	var local_url = new URL(window.location)
	var err_src =	document.querySelectorAll('.error')
	for(let err_src_tested of err_src) {
		var src_tested_query = new URL(err_src_tested.href).searchParams.get('q')
		var src_tested_name = err_src_tested.id
		sources_tested(src_tested_name, src_tested_query, local_url)
	}
	results_test(false)
}
var dateBefore, dateAfter
async function test_sources(provided_sources) {
	var all_sources = provided_sources
	var local_url = new URL(window.location)
	local_url = local_url.origin + local_url.pathname
	local_url = new URL(local_url)
	dateBefore = new Date().getTime()
	for(let src_tested of  Object.keys(all_sources)) {
		var src_tested_name = all_sources[src_tested]['tags']['name']
		var src_tested_query = all_sources[src_tested].positive_test_search_term ?
			all_sources[src_tested].positive_test_search_term : "Quadrature du net"
		sources_tested(src_tested_name, src_tested_query, local_url)
	}
}
function results_test(speed_test) {
	var pool = parseInt(
		document.getElementById('mp_display_result_test').getAttribute('data-pool'))
	if(pool==400) {// when pool == 400, all tab for test are close, test is finished
		if(speed_test) {
			dateAfter = new Date().getTime()
			console.log((dateAfter - dateBefore)/1000)
		}
		var success = document.querySelectorAll('.success').length
		var warning = document.querySelectorAll('.warning').length
		var error = document.querySelectorAll('.error').length
		let p = document.getElementById('mp_test_results')
		p.style = "display:inline"
		p.innerHTML = `Success: ${success}, Warning: ${warning}, Error: ${error}`
		var retest_btn = document.getElementById('mp_retest')
		retest_btn.style = "display:inline;"
		retest_btn.addEventListener('click',retest_sources)
	}
	else { setTimeout(function () {results_test(speed_test)}, 200)}
}
/*	 */

(async () => {
	'use strict'
	/* * definitions * */
	const MAX_RES_BY_SRC = await mµ.get_stored_max_res_by_src()
	var HEADLINE_PAGE_SIZE = await mµ.get_stored_headline_page_size()
	const HEADLINE_TITLE_SIZE = 140  // charaters
	const META_FINDINGS_PAGE_SIZE = 15
	// const EXCERPT_SIZE = 350	// charaters
	var meta_press_css = document.styleSheets[1]
	const SEL_MOD_CSS_RULE_IDX = meta_press_css.rules.length
	meta_press_css.insertRule(`.f_selection {display:none}`, SEL_MOD_CSS_RULE_IDX)
	const userLang = await mµ.get_wanted_locale()
	var sources_objs = {}
	var sources_keys = []
	var current_source_selection = []
	var current_source_nb = 0
	mp_req = {
		findingList: new List('findings', {  // Definition of a result record in List.js
			item: 'finding-item',
			valueNames: ['f_h1', 'f_txt', 'f_source', 'f_dt', 'f_by',
				{ name: 'f_source_title', attr: 'title' },
				{ name: 'mp_data_source', attr: 'mp-data-src'},
				{ name: 'f_by_title', attr: 'title' },
				{ name: 'f_ISO_dt', attr: 'datetime' },
				{ name: 'f_h1_title', attr: 'title' },
				{ name: 'f_dt_title', attr: 'title' },
				{ name: 'f_img_src', attr: 'src'},
				{ name: 'f_img_alt', attr: 'alt'},
				{ name: 'f_img_title', attr: 'title'},
				{ name: 'mp_data_img', attr: 'mp-data-img'},
				{ name: 'mp_data_alt', attr: 'mp-data-alt'},
				{ name: 'mp_data_title', attr: 'mp-data-title'},
				// { name: 'f_txt_title', attr: 'title' },
				{ name: 'f_url', attr: 'href' },
				{ name: 'f_icon', attr: 'src' },
			],
			page: 10,
			pagination: { outerWindow: 2, innerWindow: 3 },
			// fuzzySearch: { distance: 1501 }, // Unused. 1000 would search through 400 char.
		}),
		metaFindingsList: new List('meta-findings', { // List of source related info for a query
			item: 'meta-findings-item',
			valueNames: [
				{ name: 'mf_icon', attr: 'src' },
				'mf_name',
				{ name: 'mf_title', attr: 'title' },
				{ name: 'mf_ext_link', attr: 'href' },
				{ name: 'mf_remove_source', attr: 'data-mf_remove_source' },
				{ name: 'mf_res_nb', attr: 'data-mf_res_nb' },
				'mf_locale_res_nb',
			],
			page: META_FINDINGS_PAGE_SIZE,
			pagination: true
		}),
		running_query_countdown: 0,
		final_result_displayed: 0,
		query_result_timeout: null,
		query_final_result_timeout: null,
		query_start_date: null,
	}
	mp_req.findingList.on('updated', () => {
		for (let a of document.querySelectorAll(`[src="${img_load}"]`)) {
			a.removeEventListener('click', load_result_image)
			a.addEventListener('click', load_result_image, {once: true})
		}
	})
	function load_result_image (e) {
		var mp_data_src = e.target.attributes['mp-data-img'].textContent
		var mp_data_alt = e.target.attributes['mp-data-alt'] ?
			e.target.attributes['mp-data-alt'].textContent : ''
		var mp_data_title = e.target.attributes['mp-data-title'] ?
			e.target.attributes['mp-data-title'].textContent : ''
		for (let img of document.querySelectorAll(`img[mp-data-img='${mp_data_src}']`)) {
			img.src = mp_data_src
			img.alt = mp_data_alt
			img.title = mp_data_title
		}
	}
	function image_not_load (list, src, alt, title) {
		list['mp_data_img'] = src
		list['mp_data_alt'] = alt
		list['mp_data_title'] = title
		list['f_img_src'] = img_load
		list['f_img_alt'] = alt_load_txt
		list['f_img_title'] = ''
	}
	async function separate_txt_img (l_fmp, f_img, n) {
		//console.log(f_img) //to find attributes to select
		if (f_img == '' || !f_img) {
			f_img = µ.regextract('(<img [^>]*>)', l_fmp['f_txt'])
			l_fmp['f_txt'] = l_fmp['f_txt'].replace(new RegExp('(<img [^>]*>)(</img>)?', 'gm'), '')
		}
		if (l_fmp['f_img_src'] == '') {
			let src_attr = µ.extract_val_attr('src', f_img)
			if (src_attr != '') { l_fmp['f_img_src'] = µ.urlify(src_attr, n.domain_part) }
		}
		if (l_fmp['f_img_alt'] == '') l_fmp['f_img_alt'] = µ.extract_val_attr('alt', f_img)
		if (l_fmp['f_img_title'] == '') l_fmp['f_img_title'] = µ.extract_val_attr('title', f_img)
		if (!await mµ.get_stored_load_photos() && l_fmp['f_img_src']) {
			image_not_load(l_fmp, l_fmp['f_img_src'], l_fmp['f_img_alt'], l_fmp['f_img_title'])
		}
		return l_fmp
	}
	document.getElementById('mp_query').value = new URL(window.location).searchParams.get('q')
	document.getElementById('mp_query').addEventListener('keypress', function(evt) {
		if (!evt) evt = window.event
		var keyCode = evt.keyCode || evt.which
		if (keyCode == 13) {	// search on pressing Enter key
			document.getElementById('mp_submit').click()
			return false	// returning false will prevent the event from bubbling up.
		}
	})
	if(await mµ.get_stored_sentence_search()) {
		document.getElementById('mp_query').addEventListener('input', () => {
			let query = document.getElementById('mp_query').value
			let mw_query = new RegExp('[^ ]  *[^ ]')
			let mw_tech = tag_select_multiple['tech']
			if (0 < tag_select_multiple['name'].getValue(true).length) {
				mw_tech.removeActiveItemsByValue('one word')
				mw_tech.removeActiveItemsByValue('many words')
			} else {
				if(mw_query.test(query)) {
					mw_tech.removeActiveItemsByValue('one word')
					mw_tech.setChoiceByValue('many words')
				} else {
					mw_tech.removeActiveItemsByValue('many words')
					mw_tech.setChoiceByValue('one word')
				}
			}
			on_tag_sel({})
		})
	}
	function request_permissions(get_current_source_hosts) {
		return browser.permissions.request(
			{origins: get_current_source_hosts})
	}
	function alert_perm() {
		alert(mp_i18n.gettext(
			`Meta-Press.es needs the permission to read the content of the selected sources.
Those 'host' permissions can be dropped or requested once and for all via the
Meta-Press.es setting page.`))
	}
	async function tick_query_duration() {
		document.getElementById('mp_running_duration').textContent =
			(new Date() - mp_req.query_start_date) / 1000
		mp_req.duration_ticker = setTimeout(tick_query_duration, 1000)
	}
	function no_err(name, msg) {
		if(test_src==1) {
			var opener_link  = opener.document.getElementById(name)
			if(opener_link.getAttribute('class') != 'error' &&
				opener_link.getAttribute('class') != 'warning')
			{
				opener_link.innerHTML = "V"
				opener_link.setAttribute('class', 'success')
				opener_link.getAttribute('title')
				opener_link.setAttribute('title', msg)
			}
		}
	}
	function add_err(error,name,statut) {
		if(test_src==1) {
			var opener_link  = opener.document.getElementById(name)
			opener_link.innerHTML = "X"
			if(opener_link.getAttribute('class')!='error') {
				opener_link.setAttribute('class', statut)
			}
			var title = opener_link.getAttribute('title')
			if(title!="") {opener_link.setAttribute('title',title +'\n'+ error)}
			else {opener_link.setAttribute('title',error)}
		}
	}
	document.getElementById('mp_submit').addEventListener('click', async () => {
		mp_req.search_terms = document.getElementById('mp_query').value
		if (mp_req.running_query_countdown != 0 || '' == mp_req.search_terms)
			return // to prevent 2nd search during a running one
		mp_req.running_query_countdown = 1 // to prevent 2nd search during permission request
		try {
			let search_host = mµ.get_current_source_search_hosts(
				current_source_selection, sources_objs)
			if (!await request_permissions(search_host)) {
				alert_perm()
				mp_req.running_query_countdown = 0
				return
			}
		} catch (exc) {
			console.warn('Ignored exception : ', exc)
		}
		document.getElementById('mp_submit').disabled = true
		document.getElementById('mp_submit').style.cursor = 'not-allowed'
		document.getElementById('mp_stop').style.display = 'inline'
		document.getElementById('search_stats').style.display='none'
		document.getElementById('donation_reminder').style.display='none'
		document.body.style.cursor = 'wait'
		mp_req.running_query_countdown = current_source_nb
		//console.log(`Countdown refill ${current_source_nb}`);
		mp_req.query_start_date = new Date()
		mp_req.duration_ticker = setTimeout(tick_query_duration, 1000)
		clear_results()
		// for (let i of current_source_selection) {	//!\ pb with closures
		current_source_selection.forEach(async (i) => {
			var n = sources_objs[i]
			var src_query_start_date = new Date()
			try { // https://yashints.dev/blog/2019/08/17/js-async-await
				var raw_rep = await fetch(format_search_url(n.search_url, mp_req.search_terms), {
					method: n.method || 'GET',
					headers: { 'Content-Type': 'application/x-www-form-urlencoded'},
					body: n.body ? format_search_url(n.body, mp_req.search_terms) : null
				})
			} catch (exc) {
				search_query_countdown()
				add_err(exc,n.tags.name,'error')
				throw new Error(`${i}: await fetch error : ${exc}`)
			}
			if (!raw_rep.ok) {
				search_query_countdown()
				add_err(`${i}: fetch not ok : ${raw_rep.status}`,n.tags.name,'error')
				throw new Error(`${i}: fetch not ok : ${raw_rep.status}`)
			}
			var c_type = raw_rep.headers.get('content-type')
			if (c_type.includes('application/json') || (n.type && n.type == 'JSON')) {
				try {
					if(n.jsonp_to_json_re) {
						rep = µ.regextract(
							n.jsonp_to_json_re[0], await raw_rep.text() , n.jsonp_to_json_re[1])
						rep = JSON.parse(rep)
					} else { rep = await raw_rep.json() }
				} catch (exc) {
					search_query_countdown()
					add_err(exc,n.tags.name,'error')
					throw new Error(`${i} await raw_rep.json() error : ${exc} ${raw_rep}`)
				}
			} else {
				if (n.tags.charset) {
					raw_rep = await raw_rep.arrayBuffer()
					var text_decoder = new TextDecoder(n.tags.charset, {fatal: true})
					raw_rep = text_decoder.decode(raw_rep)
				} else {
					raw_rep = await raw_rep.text()
				}
				c_type = µ.clean_c_type(c_type)
				// if (n.extends == 'RSS') c_type = 'text/xml'	//!\ some sources give a wrong c_type
				var rep = µ.dom_parser.parseFromString(raw_rep, c_type)
				if (µ.isParserError(rep)) {
					if (c_type.includes('xml')) // what was the source needing this ?
						rep = µ.XML_encode_UTF8(µ.HTML_decode_entities(µ.trim(raw_rep)))
					rep = µ.dom_parser.parseFromString(rep, c_type)
					if (µ.isParserError(rep)) {
						search_query_countdown()
						add_err(`${i} dom parser error ${rep}`,n.tags.name,'error')
						throw new Error(`${i} dom parser error ${rep}`, rep)
					}
				}
			}
			if (!n.domain_part || n.extends) n.domain_part = µ.domain_part(i)
			if (!n.favicon_url && ! ('JSON' == n.type))
				n.favicon_url = µ.get_favicon_url(rep, n.domain_part)
			var fdgs = []
			if ('JSON' == n.type)
				fdgs = typeof(n.results) != 'undefined'  ? $_('results', rep, n) : rep
			else
				try { fdgs = rep.querySelectorAll(n.results)}
				catch (exc) {
					add_err(exc,n.tags.name,'error')
					console.error(i, rep, exc)
				}
			var fdgs_nb = fdgs ? fdgs.length : 0
			var res_nb = 0
			if (n.type != 'RSS' && n.type != 'ATOM') {
				try { res_nb = $_('res_nb', rep, n)}
				catch (exc) {
					add_err(exc,n.tags.name,'error')
					console.log(i, 'res_nb', rep, exc)
				}
			}
			res_nb = Number(res_nb) || fdgs_nb
			if (0 == res_nb) {add_err('No result',n.tags.name,'error')}
			var max_fdgs = MAX_RES_BY_SRC
			fdgs_nb = Math.min(fdgs_nb, max_fdgs)
			if (!fdgs_nb) return search_query_countdown()
			var f_h1='', f_url='', f_by='', f_dt=new Date(0), f_nb=0, f_img=''
			var l_fmp ={'f_img_src':'','f_img_alt':'','f_img_title':'','mp_data_img':'',
				'mp_data_alt':'','mp_data_title':'','f_txt':''}
			var dup_elt, dup_val
			for (let f of fdgs) {
				if (max_fdgs-- == 0) break
				f_nb = MAX_RES_BY_SRC - max_fdgs
				f_h1 = ''; f_url=''; l_fmp['f_txt']=''; f_by=''; f_img=''; f_dt=new Date(0)
				l_fmp['f_img_src']='', l_fmp['f_img_alt']='', l_fmp['f_img_title']='',
				l_fmp['mp_data_img']='', l_fmp['mp_data_alt']='', l_fmp['mp_data_title']=''
				try { f_h1	= $_('r_h1',f,n) }
				catch (exc) {
					add_err(exc,n.tags.name,'error')
					console.error(i, f_nb, 'f_h1', f, exc)
				}
				try { f_url = µ.urlify($_('r_url', f, n), n.domain_part) }
				catch (exc) {
					add_err(exc,n.tags.name,'error')
					console.error(i, f_nb, 'f_url', f, exc)
				}
				if (n.type == 'RSS' || n.type == 'ATOM') {
					try { l_fmp['f_txt'] = µ.no_inline_style($_('r_txt', f, n))}
					catch (exc) {
						add_err(exc,n.tags.name,'error')
						console.error(i, f_nb, 'f_txt', f, exc)
					}
					l_fmp = await separate_txt_img(l_fmp, f_img, n)
				} else {
					var f_img_src = $_('r_img_src', f, n)
					if(f_img_src != 'undefined' && f_img_src != '') {
						try { l_fmp['f_img_src'] = µ.urlify(f_img_src, n.domain_part) }
						catch (exc) {
							add_err(exc,n.tags.name,'error')
							console.error(i, f_nb, 'f_img_src', f, exc)
						}
					}
					var f_img_alt = $_('r_img_alt', f, n)
					if(f_img_alt) {
						try { l_fmp['f_img_alt'] ? f_img_alt : '' }
						catch (exc) {
							add_err(exc,n.tags.name,'error')
							console.error(i, f_nb, 'f_img_alt', f, exc)
						}
					}
					var f_img_title = $_('r_img_title', f, n)
					if(f_img_title) {
						try { l_fmp['f_img_title'] ? f_img_title : '' }
						catch (exc) {
							add_err(exc,n.tags.name,'error')
							console.error(i, f_nb, 'f_img_title', f, exc)
						}
					}
					if(n.r_img) {
						var qn_img = f.querySelector(n.r_img)
						if (qn_img) {
							try {
								f_img = qn_img.outerHTML
								l_fmp = await separate_txt_img(l_fmp, f_img, n)
							} catch (exc) {
								add_err(exc,n.tags.name,'error')
								console.error(i, f_nb, 'f_img', f, exc)
							}
						}
					} else {
						if (!await mµ.get_stored_load_photos() && l_fmp['f_img_src']) {
							image_not_load(l_fmp, l_fmp['f_img_src'], l_fmp['f_img_alt'],
								l_fmp['f_img_title'])
						}
					}
					try { l_fmp['f_txt'] = µ.no_inline_style($_('r_txt', f, n))}
					catch (exc) {
						add_err(exc,n.tags.name,'error')
						console.error(i, f_nb, 'f_txt', f, exc)
					}
				}
				try { f_by	= $_('r_by', f, n) || n.tags.name }
				catch (exc) {
					add_err(exc,n.tags.name,'error')
					console.error(i, f_nb, 'f_by', f, exc)
				}
				try { f_dt	=	$_('r_dt', f, n)
					if (f_dt == '') throw "No f_dt after lookup ($_)"
					f_dt	= parse_dt_str(f_dt, n.tags.tz, max_fdgs) }
				catch (exc) {
					add_err(exc,n.tags.name,'error')
					console.error(i, f_nb, 'f_dt', f, exc)
				}
				try {
					if (await mµ.get_stored_undup_results() && (dup_elt = get_dup(f_h1)).length) {
						dup_elt = dup_elt[0]
						dup_val = dup_elt.values()
						dup_val.f_source += dup_val.f_source.slice(-1) == '…' ? '' : '…'
						dup_val.f_source_title += `, ${n.tags.name}`
						dup_elt.values(dup_val)
					} else {
						if (f_h1 == '' && f_url == `${n.domain_part}/`) {
							res_nb -= 1
							throw 'Empty title and link'
						}
						mp_req.findingList.add({f_h1:f_h1, f_url:f_url, f_by:f_by,
							f_txt: µ.bolden(l_fmp['f_txt'], mp_req.search_terms),
							// f_txt_title:l_fmp['f_txt'] && (l_fmp['f_txt'].length > EXCERPT_SIZE) ?
							//	µ.HTML_decode_entities(
							//			µ.strip_html(l_fmp['f_txt'])) : '',
							f_h1_title:	f_h1,
							f_by_title:	f_by,
							f_dt: f_dt.toISOString().slice(0, -14),
							f_epoc:	f_dt.valueOf(),
							f_ISO_dt: f_dt.toISOString(),
							f_dt_title: f_dt.toLocaleString(userLang),
							f_icon:	n.favicon_url,
							f_img_src: l_fmp['f_img_src'],
							f_img_alt: l_fmp['f_img_alt'],
							f_img_title: l_fmp['f_img_title'],
							mp_data_img: l_fmp['mp_data_img'],
							mp_data_alt: l_fmp['mp_data_alt'],
							mp_data_title: l_fmp['mp_data_title'],
							f_source: n.tags.name,
							f_source_title: n.tags.name,
							mp_data_source: n.tags.name,
						})
					}
				} catch (exc) {
					add_err(exc,n.tags.name,'error')
					console.error(i, f_nb, f_dt, exc)
				}
			}
			no_err(n.tags.name,"No error")
			if(res_nb != 0 && !test_src) {
				mp_req.metaFindingsList.add({
					mf_icon: n.favicon_url,
					mf_name: n.tags.name,
					mf_title: mf_title(n.tags.name),
					mf_ext_link: format_search_url(n.search_url_web || n.search_url, mp_req.search_terms),
					mf_remove_source: n.tags.name,
					mf_res_nb: res_nb,
					mf_locale_res_nb: res_nb.toLocaleString(userLang),
				})
			}
			search_query_countdown()
			console.log(i, new Date() - src_query_start_date, 'ms',
				mp_req.running_query_countdown, '/', current_source_nb, ':', res_nb, 'res')
		})
	})
	function get_dup(f_h1) {
		return mp_req.findingList.get('f_h1', f_h1)
	}
	function search_query_countdown() {
		mp_req.running_query_countdown -= 1
		clearTimeout(mp_req.query_result_timeout)
		mp_req.query_result_timeout = setTimeout(() => { display_ongoing_results() }, 750)
	}
	function filter_only_one_element(elt) {
		if (elt == null) return
		mp_req.findingList.search(elt.textContent, 'f_source')
		bold_clicked_a(elt, 'mf_name')
		mf_date_slicing()
		filter_last(document.getElementById('mf_all_res'), null)
	}

	function display_ongoing_results() {
		document.getElementById('mp_query_countdown').textContent = mp_req.running_query_countdown
		document.getElementById('mp_running_src_fetched').textContent = current_source_nb
		document.getElementById('mp_running_stats').style.display = 'block'
		document.getElementsByTagName('title')[0].textContent = document.getElementById(
			'ongoing_tab_title').textContent
		if (mp_req.findingList.size() > 0) {
			mp_req.findingList.sort('f_epoc', {order: 'desc'})
			mp_req.metaFindingsList.show(0, mp_req.metaFindingsList.size())
			for (let a of document.getElementsByClassName('mf_name')) {
				// a.addEventListener('click', evt => {  // would bind multiple times the event
				a.onclick = (evt) => filter_only_one_element(evt.target)
				a.parentNode.onclick = (evt) =>
					filter_only_one_element(evt.target.querySelector('.mf_name'))
			}
			for (let a of document.getElementsByClassName('mf_remove_source')) {
				a.onclick = evt => {
					document.body.style.cursor = 'wait'
					const mf_name = evt.target.parentNode.querySelector('.mf_name')
					mf_name.onclick({target: mf_name}) // filter only the elements to delete
					const cur_page_size = mp_req.findingList.page
					mp_req.findingList.show(0, mp_req.findingList.size()) // display all the elts to del
					const source_name = evt.target.getAttribute('data-mf_remove_source')
					mp_req.findingList.remove('mp_data_source', source_name)
					mp_req.metaFindingsList.remove('mf_name', source_name)
					document.getElementById('mp_query_meta_total').textContent =
						get_mf_res_nb().toLocaleString(userLang)
					mp_req.findingList.show(0, cur_page_size)
					document.getElementById('mf_total').click()
					document.body.style.cursor = 'auto'
				}
			}
			mp_req.metaFindingsList.show(0, META_FINDINGS_PAGE_SIZE)
			mp_req.metaFindingsList.sort('mf_res_nb', {order: 'desc'})
			document.getElementById('mf_total').onclick = evt => {
				mp_req.findingList.search()
				bold_clicked_a(evt.target, 'mf_name')
				mf_date_slicing()
				filter_last(document.getElementById('mf_all_res'), null)
			}
			mf_date_slicing()
			for (let sbm of document.getElementsByClassName('sidebar-module')) {
				sbm.style.display='block'
			}
			mp_req.findingList.update()
		}
		if (! mp_req.final_result_displayed && mp_req.running_query_countdown == 0)
			display_final_results()
	}
	function get_mf_res_nb () {
		let a = 0
		for (let r of mp_req.metaFindingsList.items) a += r.values().mf_res_nb
		return a
	}
	async function display_final_results() {
		mp_req.final_result_displayed = 1
		location.href = '#'; location.href = '#before_search_stats'
		document.getElementById('mp_running_stats').style.display = 'none'
		document.getElementById('search_stats').style.display='block'
		if (µ.rnd(1) > 7)
			document.getElementById('donation_reminder').style.display='block'
		clearTimeout(mp_req.duration_ticker)
		if (0 == mp_req.findingList.size()) {
			document.getElementById('no_results').style.display = 'block'
		} else {
			document.getElementById('mp_page_sizes').style.display = 'block'
		}
		const cur_page_size = mp_req.findingList.page
		mp_req.findingList.show(0, mp_req.findingList.size())  // apply to all elements
		for (let a of document.getElementsByClassName('f_txt'))
			if (µ.isOverflown(a))
				a.style.resize = 'vertical'
		mp_req.findingList.show(0, cur_page_size)
		document.getElementById('mp_query_meta_total').textContent =
			get_mf_res_nb().toLocaleString(userLang)
		document.getElementById('mp_query_meta_local_total').textContent = mp_req.findingList.size()
		var mp_end_date = ndt()
		var req_duration = (mp_end_date - mp_req.query_start_date) / 1000
		document.getElementById('mp_duration').textContent = req_duration
		document.getElementById('mp_duration').title = mp_end_date.toLocaleString(userLang)
		document.getElementById('mp_query_search_terms').textContent = mp_req.search_terms
		document.getElementById('mp_src_nb').textContent =
			mp_req.metaFindingsList.items.filter(i => i.values().mf_res_nb > 0).length
		document.getElementById('mp_src_fetched').textContent = current_source_nb
		document.getElementById('mp_submit').disabled = false
		document.getElementById('mp_submit').style.cursor = 'pointer'
		document.getElementById('mp_stop').style.display = 'none'
		document.getElementsByTagName('title')[0].textContent = document.getElementById(
			'tab_title').textContent
		document.body.style.cursor = 'auto'
		let id_sch_s = cur_querystring.get('id_sch_s')
		if (req_duration > 8 && ! id_sch_s) mµ.notifyUser(
			document.getElementById('tab_title').textContent,
			µ.triw(document.getElementById('search_stats').textContent).replace(' 🔗', '')
		)
		if (!await mµ.get_stored_keep_host_perm())
			mµ.drop_host_perm()
		if(id_sch_s)
			add_elt_url(id_sch_s)
		// browser.permissions.remove({origins:
		//	mµ.get_current_source_hosts(current_source_selection)})
		if(test_src == '1') {
			let elt = opener.document.getElementById('mp_display_result_test')
			let cur_url = new URL(window.location)
			let name = cur_url.searchParams.get('name')
			cur_url.searchParams.set('test_sources','0')
			opener.document.getElementById(name).href = cur_url
			let pool = parseInt(elt.getAttribute('data-pool')) + 1
			elt.setAttribute('data-pool',pool)
			self.close()
		} else {
			document.getElementById('mp_search_permalink').href = µ.rm_href_anchor(
				gen_permalink(true))
		}
	}
	function clear_results() {
		mp_req.final_result_displayed = 0
		document.getElementById('no_results').style.display = 'none'
		document.getElementById('mp_page_sizes').style.display = 'none'
		document.getElementById('mp_query_meta_total').textContent = '…'
		document.getElementById('mp_query_meta_local_total').textContent = '…'
		document.getElementById('mp_running_stats').style.display = 'none'
		document.getElementById('mp_query_countdown').textContent = '…'
		document.getElementById('mp_running_src_fetched').textContent = '…'
		document.getElementById('mp_duration').textContent = '…'
		document.getElementById('mp_query_search_terms').textContent = '…'
		document.getElementById('mp_src_nb').textContent = '…'
		document.getElementById('mp_src_fetched').textContent = '…'
		document.getElementById('mp_duration').textContent = '…'
		mp_req.findingList.clear()
		mp_req.metaFindingsList.clear()
		document.getElementById('mp_clear_filter').click()
	}
	document.getElementById('mp_autosearch_permalink').addEventListener('click', () => {
		var sch_sea = gen_permalink(false)
		sch_sea = sch_sea.toString() // Chrome doesn't support URL in browser.storage elt
		browser.storage.sync.set({new_sch_sea: sch_sea})
	})
	document.getElementById('mp_stop').addEventListener('click', () => {
		// cancel running promises -> impossible in 2019
		// clear_results()
		reload_keeping_query()
	})
	function reload_keeping_query() {
		// location.reload()
		window.location = µ.rm_href_anchor(gen_permalink(false))
	}
	function gen_permalink(is_submit) {
		let permalink = new URL(window.location)
		permalink.searchParams.set('q', document.getElementById('mp_query').value)
		for (let t of Object.keys(tag_select_multiple)) {
			permalink.searchParams.delete(t)
			for (let v of tag_select_multiple[t].getValue(true)) {
				permalink.searchParams.append(t, v)
			}
		}
		if (is_submit) {
			permalink.searchParams.set('submit', 1)
		} else {
			permalink.searchParams.delete('submit')
		}
		return permalink
	}
	const HEADLINES_H_RATIO = 21
	const HEADLINES_H_CONST = 64
	const headlines_div = document.getElementById('headlines')
	for (let pg_size of [5, 10, 20, 50])
		document.getElementById(`mp_headlines_page_${pg_size}`).addEventListener('click', () => {
			HEADLINE_PAGE_SIZE=pg_size
			headlineList.show(0, HEADLINE_PAGE_SIZE)
			let hdl_shown_nb = Math.min(pg_size, headlineList.visibleItems.length)
			headlines_div.style.height = `${hdl_shown_nb * HEADLINES_H_RATIO + HEADLINES_H_CONST}px`
		})
	document.getElementById('mp_headlines_page_all').addEventListener('click', () => {
		HEADLINE_PAGE_SIZE=headlineList.size(); headlineList.show(0, HEADLINE_PAGE_SIZE)})
	for (let pg_size of [10, 20, 50])
		document.getElementById(`mp_page_${pg_size}`).addEventListener('click', () => {
			mp_req.findingList.show(0, pg_size)})
	document.getElementById('mp_page_all').addEventListener('click', () => {
		mp_req.findingList.show(0, mp_req.findingList.size())})
	document.getElementById('mp_import_JSON').addEventListener('click', () => {
		let input_elt = document.createElement('input')
		input_elt.type = 'file'
		input_elt.click()
		input_elt.addEventListener('change', () => {
			document.body.style.cursor = 'wait'
			window.scroll(0, 0)
			mp_req.query_start_date = ndt()
			let file = input_elt.files[0]
			if (file) {
				let reader = new FileReader()
				reader.readAsText(file, 'UTF-8')
				reader.onload = function (evt) {
					try {
						let input_json = JSON.parse(evt.target.result)
						tags_fromJSON(input_json.filters)
						clear_results()
						mp_req.findingList.add(input_json.findings)
						mp_req.metaFindingsList.add(input_json.meta_findings)
						document.getElementById('mp_query').value = input_json.search_terms
						display_ongoing_results()
						document.body.style.cursor = 'auto'
					} catch (exc) {
						console.error("JSON loading failure", exc)
						document.body.style.cursor = 'auto'
					}
				}
				reader.onerror = () => console.error('error importing JSON file')
			}
		}, false)
	})
	document.getElementById('mp_import_RSS').addEventListener('click', e=>import_XML(e, 'RSS'))
	document.getElementById('mp_import_ATOM').addEventListener('click',e=>import_XML(e,'ATOM'))
	function import_XML(evt, flux_type) {
		let input_elt = document.createElement('input')
		input_elt.type = 'file'
		input_elt.click()
		input_elt.addEventListener('change', () => {
			document.body.style.cursor = 'wait'
			mp_req.query_start_date = ndt()
			let file = input_elt.files[0]
			if (file) {
				let reader = new FileReader()
				reader.readAsText(file, 'UTF-8')
				reader.onload = async function (evt) {
					try {
						let input_xml = µ.dom_parser.parseFromString(evt.target.result, 'application/xml')
						clear_results()
						var r_src, n, r_dt, r_by, f_source, f_icon, item_obj, f_h1, f_url, f_img, l_fmp
						var mf = {}
						var p = XML_SRC[flux_type] // parser info
						tags_fromXML(input_xml.querySelectorAll(p.filters), flux_type)
						mp_req.search_terms = input_xml.querySelector('title').textContent.replace(
							'Meta-Press.es : ', '')
						document.getElementById('mp_query').value = mp_req.search_terms
						for (let item of input_xml.querySelectorAll(p.results)) {
							l_fmp ={'f_img_src':'','f_img_alt':'','f_img_title':'','mp_data_img':'',
								'mp_data_alt':'','mp_data_title':'','f_txt':''}
							f_url = $_('r_url', item, p)
							r_src = µ.domain_part(f_url)
							n = provided_sources[r_src]
							if (n && !n.domain_part) n.domain_part = r_src
							// if (!n.favicon_url) n.favicon_url =
							//	µ.get_favicon_url(rep, n.domain_part);
							// console.log('date ',$_('r_dt', item, p))
							r_dt = new Date($_('r_dt', item, p))
							r_by = $_('r_by', item, p)
							f_source = n && n.tags.name || µ.domain_name(f_url) || ''
							f_icon = n && n.favicon_url || '/img/Feed-icon.svg'
							l_fmp['f_txt'] = µ.no_inline_style(µ.triw($_('r_txt', item, p)))
							l_fmp = await separate_txt_img(l_fmp, f_img, n || {domain_part: r_src})
							f_h1 = $_('r_h1', item, p)
							item_obj = {
								f_h1: f_h1,
								f_h1_title: f_h1,
								f_url: f_url,
								f_icon: f_icon,
								f_img_src: l_fmp['f_img_src'],
								f_img_alt: l_fmp['f_img_alt'],
								f_img_title: l_fmp['f_img_title'],
								mp_data_img: l_fmp['mp_data_img'],
								mp_data_alt: l_fmp['mp_data_alt'],
								mp_data_title: l_fmp['mp_data_title'],
								f_txt: l_fmp['f_txt'],
								f_by: r_by,
								f_by_title: r_by,
								f_dt: r_dt.toISOString().slice(0, -14),
								f_dt_title: r_dt.toLocaleString(userLang),
								f_epoc: r_dt.valueOf(),
								f_ISO_dt: r_dt.toISOString(),
								f_source: f_source,
								f_source_title: f_source,
								mp_data_source: f_source
							}
							mp_req.findingList.add(item_obj)
							if (typeof(mf[r_src]) == 'undefined') {
								mf[r_src] = {
									mf_icon: f_icon,
									mf_name: f_source,
									mf_title: mf_title(f_source),
									mf_ext_link: n && format_search_url(n.search_url_web || n.search_url,
										mp_req.search_terms) || '',
									mf_remove_source: f_source,
									mf_res_nb: 0
								}
							}
							mf[r_src].mf_res_nb += 1
							mf[r_src].mf_locale_res_nb = mf[r_src].mf_res_nb.toLocaleString(userLang)
						}
						var v
						for (let k of Object.keys(mf)) {
							v = mf[k]
							mp_req.metaFindingsList.add(v)
						}
						document.body.style.cursor = 'auto'
						display_ongoing_results()
					} catch (exc) {
						document.body.style.cursor = 'auto'
						console.error(flux_type, "loading failure", exc)
					}
				}
				reader.onerror = () => { console.error(`error importing ${flux_type} file`) }
			}
		}, false)
	}
	function mf_title(src_name) {
		return `${mp_i18n.gettext('Filter results of')} '${src_name}' ${mp_i18n.gettext('only')}`
	}
	document.getElementById('mp_sel_mod').addEventListener('click', () => {
		let cur_sel_mod = meta_press_css.rules[SEL_MOD_CSS_RULE_IDX].cssText.match(
			/(block|none)/)[0]
		let next_sel_mod = cur_sel_mod == 'none' ? 'block' : 'none'
		document.getElementsByClassName('mp_export_choices')[0].style.display = next_sel_mod
		meta_press_css.deleteRule(SEL_MOD_CSS_RULE_IDX)
		meta_press_css.insertRule(
			`.f_selection {display:${next_sel_mod}!important}`, SEL_MOD_CSS_RULE_IDX)
	})
	document.getElementById('mp_export_sel_JSON').addEventListener('click', () => {
		mp_req.findingList.filter(i => {
			if (i.elm) return i.elm.getElementsByClassName('f_selection')[0].checked})
		let cur_sel_items = []
		for (let i of mp_req.findingList.matchingItems) cur_sel_items.push(i.values())
		mp_req.findingList.filter()
		let json_str = `{
			"filters": ${JSON.stringify(tags_toJSON())},
			"findings": ${JSON.stringify(cur_sel_items)},
			"meta_findings": ${JSON.stringify(mp_req.metaFindingsList.toJSON())},
			"search_terms": ${JSON.stringify(document.getElementById('mp_query').value)}
		}`
		µ.upload_file_to_user(`${µ.ndt_human_readable()}_meta-press.es.json`, json_str)
	})
	document.getElementById('mp_export_sel_RSS').addEventListener('click', () => {
		mp_req.findingList.filter(i => {
			if (i.elm) return i.elm.getElementsByClassName('f_selection')[0].checked})
		export_RSS()
		mp_req.findingList.filter()
	})
	document.getElementById('mp_export_search_RSS').addEventListener('click', () => {
		export_RSS()
	})
	function export_RSS() {
		document.body.style.cursor = 'wait'
		// https://validator.w3.org/feed/#validate_by_input
		let flux_items = '', v = {}, img=''
		for (let i of mp_req.findingList.matchingItems) {
			v = i.values()
			if (v.f_img_src) {
				if (v.f_img_alt == alt_load_txt) {
					img = mµ.img_tag(v.mp_data_img, v.mp_data_alt, v.mp_data_title)
				} else {
					img = mµ.img_tag(v.f_img_src, v.f_img_alt, v.f_img_title)
				}
			} else { img = '' }
			flux_items += `<item>
				<title>${µ.encode_XML(title_line(v))}</title>
				<description>${µ.encode_XML(img)}${µ.encode_XML(v.f_txt)}</description>
				<link>${µ.encode_XML(v.f_url)}</link>
				<pubDate>${new Date(v.f_ISO_dt).toUTCString()}</pubDate>
				<dc:creator>${µ.encode_XML(v.f_by)}</dc:creator>
				<guid>${µ.encode_XML(v.f_url)}</guid>
			</item>`
		}
		let flux = `<?xml version='1.0' encoding='UTF-8'?>
		<rss version='2.0' xmlns:dc='http://purl.org/dc/elements/1.1/'>
			<channel>
				<title>Meta-Press.es : ${document.getElementById('mp_query').value}</title>
				<description>Selection RSS 2.0</description>
				<link>https://www.meta-press.es</link>
				<lastBuildDate>${ndt().toUTCString()}</lastBuildDate>
				<language>${tag_select_multiple.lang.getValue(true) || userLang}</language>
				<generator>https://www.meta-press.es</generator>
				<docs>http://www.rssboard.org/rss-specification</docs>
				${tags_toXML('RSS')}
				${flux_items}
			</channel>
		</rss>`
		document.body.style.cursor = 'auto'
		µ.upload_file_to_user(`${µ.ndt_human_readable()}_meta-press.es.rss`, flux)
	}
	document.getElementById('mp_export_sel_ATOM').addEventListener('click', () => {
		// https://tools.ietf.org/html/rfc4287
		// https://validator.w3.org/feed/#validate_by_input
		mp_req.findingList.filter(i => {
			if (i.elm) return i.elm.getElementsByClassName('f_selection')[0].checked})
		let flux_items = '', v = {}, img=''
		for (let i of mp_req.findingList.matchingItems) {
			v = i.values()
			if (v.f_img_src) {
				if (v.f_img_alt == alt_load_txt) {
					img = mµ.img_tag(v.mp_data_img, v.mp_data_alt, v.mp_data_title)
				} else {
					img = mµ.img_tag(v.f_img_src, v.f_img_alt, v.f_img_title)
				}
			} else { img = '' }
			flux_items += `<entry>
				<title>${µ.encode_XML(title_line(v))}</title>
				<summary type='html'>${µ.encode_XML(img)}${µ.encode_XML(v.f_txt)}</summary>
				<published>${v.f_ISO_dt}</published>
				<updated>${v.f_ISO_dt}</updated>
				<link href='${µ.encode_XML(v.f_url)}'/>
				<author><name>${v.f_by}</name></author>
				<id>${µ.encode_XML(v.f_url)}</id>
			</entry>`
		}
		mp_req.findingList.filter()
		let flux = `<?xml version='1.0' encoding='UTF-8'?>
		<feed xmlns='http://www.w3.org/2005/Atom'>
			<title>Meta-Press.es : ${document.getElementById('mp_query').value}</title>
			<subtitle>Decentralize press search engine</subtitle>
			<link href='https://www.meta-press.es'/>
			<updated>${ndt().toISOString()}</updated>
			<author><name>Meta-Press.es</name></author>
			<id>https://www.meta-press.es/</id>
			${tags_toXML('ATOM')}
			${flux_items}
		</feed>`
		µ.upload_file_to_user(`${µ.ndt_human_readable()}_meta-press.es.atom`, flux)
	})
	function title_line(v) {
		var src_prefix = `[${v.f_source}]`
		return v.f_h1.indexOf(src_prefix) != -1 ? `${src_prefix} ${v.f_h1}` : `${v.f_h1}`
	}
	function ndt() { return new Date() }
	const now = ndt()
	var date_filters = [
		function mf_last_day(r)  {return r.values().f_epoc>ndt().setDate(now.getDate()-1)},
		function mf_last_2d(r)	 {return r.values().f_epoc>ndt().setDate(now.getDate()-2)},
		function mf_last_3d(r)	 {return r.values().f_epoc>ndt().setDate(now.getDate()-3)},
		function mf_last_week(r) {return r.values().f_epoc>ndt().setDate(now.getDate()-7)},
		function mf_last_2w(r)	 {return r.values().f_epoc>ndt().setDate(now.getDate()-14)},
		function mf_last_month(r){return r.values().f_epoc>ndt().setMonth(now.getMonth()-1)},
		function mf_last_2m(r){return r.values().f_epoc>ndt().setMonth(now.getMonth()-2)},
		function mf_last_6m(r){return r.values().f_epoc>ndt().setMonth(now.getMonth()-6)},
		function mf_last_1y(r){return r.values().f_epoc>ndt().setFullYear(now.getFullYear()-1)},
		function mf_last_2y(r){return r.values().f_epoc>ndt().setFullYear(now.getFullYear()-2)},
		function mf_last_5y(r){return r.values().f_epoc>ndt().setFullYear(now.getFullYear()-5)},
		function mf_last_dk(r){return r.values().f_epoc>ndt().setFullYear(now.getFullYear()-10)}
	]
	for (let flt of date_filters)
		document.getElementById(flt.name).addEventListener('click',
			evt => filter_last(evt.target, flt))
	document.getElementById('mf_all_res').addEventListener('click',
		evt => filter_last(evt.target, null))
	function mf_date_slicing() {
		var prev_nb = 0
		var total_nb = set_filter_nb(null, 'mf_all_res', 0, 0)
		for (let flt of date_filters) {
			prev_nb = set_filter_nb(flt, flt.name, prev_nb, total_nb)
		}
		mp_req.findingList.filter()
	}
	function set_filter_nb(fct, id, prev_nb, total_nb) {
		mp_req.findingList.filter()
		if(fct) mp_req.findingList.filter(fct)
		var nb = mp_req.findingList.matchingItems.length
		if (nb > 0 && nb > prev_nb + (total_nb * 0.06)) {
			let p = Math.floor(nb * 40 / mp_req.findingList.size())
			document.getElementById(id+'_nb').textContent =
				`${nb.toLocaleString(userLang)} ${'.'.repeat(p)}`
			document.getElementById(id).parentNode.style.display='block'
		} else {
			document.getElementById(id).parentNode.style.display='none'
		}
		return nb
	}
	function filter_last(target, fct) {
		mp_req.findingList.filter()
		if(fct) mp_req.findingList.filter(fct)
		bold_clicked_a(target, 'mf_dt_lbl')
	}
	function bold_clicked_a(target, a_class) {
		for (let i of document.getElementsByClassName(a_class))
			i.classList.remove('bold')
		target.classList.add('bold')
	}
	document.getElementById('mp_filter').addEventListener('keyup', searchInResCallback)
	document.getElementById('mp_clear_filter').addEventListener('click', () => {
		document.getElementById('mp_filter').value = ''
		searchInResCallback({target: {value: ''}})
	})
	var searchInResCallback_timeout
	function searchInResCallback (evt) {
		clearTimeout(searchInResCallback_timeout)
		searchInResCallback_timeout = setTimeout(() => {
			if (mp_req.findingList.searched) { document.getElementById('mf_total').click() }
			if (mp_req.findingList.filtered) {filter_last(document.getElementById('mf_all_res'),
				null) }
			if (evt.target.value) {
				mp_req.findingList.search(evt.target.value, ['f_h1', 'f_source', 'f_dt', 'f_by',
					'f_txt'])
			} else {
				mp_req.findingList.search()
			}
			mf_date_slicing()
		}, evt.keyCode == 13 ? 0 : 500)
	}
	function parse_dt_str(dt_str, tz, f_nb) {
		var d = undefined
		if (/\d{12,20}/i.test(dt_str)) {
			d = new Date(Number(dt_str))
		} else if (/min(utes?)?( ago)?/i.test(dt_str)) {
			d = µ.timezoned_date('', tz)
			d.setMinutes(d.getMinutes() - Number(dt_str.replace(/min(utes?)? ago/, '')))
		} else if (/hours?( ago)?/i.test(dt_str)) {
			d = µ.timezoned_date('', tz)
			d.setHours(d.getHours() - Number(dt_str.replace(/hours? ago/, '')))
		} else if (/il y a \d+h/i.test(dt_str) || / \d+ h/i.test(dt_str)) {
			d = µ.timezoned_date('', tz)
			d.setHours(d.getHours() - Number(dt_str.replace(/[^\d]*(\d+)[^\d]*/, '$1')))
		} else if (/(today|new)/i.test(dt_str)) {
			d = µ.timezoned_date('', tz)
			d.setMinutes(0)
			d.setSeconds(0)
		} else if (/yesterday|hier/i.test(dt_str)) {
			d = µ.timezoned_date('', tz)
			d.setDate(d.getDate() - 1)
			d.setMinutes(0)
			d.setSeconds(0)
		} else if (/ \d+ d/i.test(dt_str) || / \d+ j/i.test(dt_str)) {
			// change day, work for dias, day, jours
			d = µ.timezoned_date('', tz)
			d.setDate(d.getDate() - Number(dt_str.replace(/.* (\d+) .*/, '$1')))
		/*} else if (µ.HH_MM_RE.test(dt_str)) {
			dt_str = dt_str.replace(/{(.*)}/, (_, $1) => month_nb($1, month_nb_json))
			try {
				d = µ.timezoned_date(dt_str, tz)
			} catch (exc) {
				console.log(`'${dt_str}' failed to be converted to new Date() with hours`)
				d = µ.timezoned_date(0, tz)
			}
			d.setMinutes(0)
			d.setSeconds(0)*/
		} else {
			if (typeof(dt_str) == 'string')
				dt_str = dt_str.replace(/{(.*)}/, (_, $1) => month_nb($1, month_nb_json))
			if (µ.MM_DD_ONLY_RE.test(dt_str)) {
				let today = µ.timezoned_date('', tz)
				dt_str = `${today.getFullYear()}${dt_str}`
			} else if (µ.HH_MM_ONLY_RE.test(dt_str) ) {
				let today = µ.timezoned_date('', tz)
				dt_str = `${today.getFullYear()}-${today.getMonth()}-${today.getDate()} ${dt_str}`
			}
			d = µ.timezoned_date(dt_str, tz)
		}
		if (!d.getMinutes() && !d.getSeconds()) {  // no time set ? put random one
			if (µ.HH_MM_RE.test(dt_str)) {
				let d_split = µ.regextract(µ.HH_MM_STR, dt_str).split(':')
				d.setHours(Number(d_split[0]))
				d.setMinutes(Number(d_split[1]))
			} else {
				// d.setHours(Math.min(ndt().getHours(),
				//	µ.rnd(1)+µ.rnd(1)+(Math.floor((µ.rnd(1)+1)/2))))
				d.setHours(0)
				d.setMinutes(f_nb % 60)
			}
		}
		if (isNaN(d))
			throw new Error(`'${dt_str}' (${tz}) is an invalid new Date()`)
		return d
	}
	function format_search_url(search_url, token) {
		return search_url.replace('{}', token)
			.replace('{#}', MAX_RES_BY_SRC)
			.replace('{+}', token.replace(' ', '+'))
		//		.replace('{%}', encodeURIComponent(token))
	}
	function $_(elt, dom, src) {	// parsing lookup
		var val = ''
		var src_elt = src[elt]
		//console.log('src_elt', src_elt)
		if(elt!= 'search_url') {
			if (src.type && src.type == 'JSON' && elt != 'headline_selector' && elt != 'h_title') {
				if (typeof(src_elt) == 'undefined') return ''  // we need undef src_elt to seek _xpath
				if (µ.is_array(src_elt)) {
					try {
						let val_lst = []
						for (let val_idx of src_elt)
							val_lst.push(µ.deref_json_path(val_idx, dom) || '')
						val = µ.str_fmt(src[`${elt}_tpl`], val_lst)
					} catch (exc) {
						add_err(`${elt} - ${exc}`,src.tags.name,'warning')
						console.error(src.tags.name, elt, exc)
					}
				} else {
					try {
						val = µ.deref_json_path(src_elt, dom)
						if (elt != 'results' && µ.is_array(val)) {	// if wanted elt 'results' return them
							let val_acc = ''
							let val_attr = src[`${elt}_attr`]
							for (let v of val) {
								if (typeof(val_attr) == 'string')
									val_acc += `${v[val_attr]}, `
								else
									val_acc += v
							}
							val = val_acc
						}
					} catch (exc) {
						add_err(`${elt} - ${exc}`,src.tags.name,'warning')
						console.error(src.tags.name, elt, exc, dom)
					}
				}
			} else {
				if (µ.is_array(src_elt)) {
					try {
						let val_lst = []
						let val_attr = src[`${elt}_attr`]
						for (let val_idx of src_elt) {
							val_lst.push(dom.querySelector(val_idx))
							if (µ.is_array(val_attr) && val_attr[val_lst.length-1]) {
								val_lst[val_lst.length-1] = val_lst[val_lst.length-1].getAttribute(
									val_attr[val_lst.length-1])
							} else {
								val_lst[val_lst.length-1] = µ.triw(val_lst[val_lst.length-1].textContent)
							}
						}
						val = µ.str_fmt(src[`${elt}_tpl`], val_lst)
					} catch (exc) {
						add_err(`${elt} - ${exc}`,src.tags.name,'warning')
						console.error(src.tags.name, elt, exc)
					}
				} else if (typeof(src_elt) == 'string') {
					val = dom.querySelector(src_elt)
				} else {
					let elt_xpath = src[`${elt}_xpath`]
					if (typeof(elt_xpath) == 'string') {
						val = µ.evaluateXPath(dom, elt_xpath)
					} else { // console.error(
						return '' // `Missing ${elt} (or ${elt}_xpath) in ${src.tags.name}, or not string.`)
					}
				}
				var src_elt_attr = src[`${elt}_attr`]
				if (typeof (src_elt_attr) == 'string') {
					try {
						val = val.getAttribute(src_elt_attr)
					} catch (exc) {
						add_err(`${elt} - ${exc}`,src.tags.name,'warning')
						console.error(src.tags.name, src_elt_attr, val, exc, dom)
						val = ''
					}
				} else if (! µ.is_array(src_elt)) {
					try {
						val = µ.triw(val.textContent)
					} catch (exc) {
						add_err(`${elt} - ${exc}`,src.tags.name,'warning')
						console.error(src.tags.name, elt, exc, dom)
						val = (elt == 'r_dt') ? '100000000000' : ''
						// it's an old date : 1973-03-03 09:46:40 GMT+0000
					}
				}
			}
		}
		//if (elt == 'r_dt') console.log('before', val)
		var elt_re = src[`${elt}_re`]  // it should be a list of 2 elements
		if (typeof (elt_re) == 'object') {
			if(typeof (val) == 'string') {
				val = µ.regextract(elt_re[0], val, elt_re[1])
			} else {
				let val_type = typeof (val)
				val = String(val)
				val = µ.regextract(elt_re[0], val, elt_re[1])
				if(val_type == 'number') { val = Number(val) }
			}
		}
		//if (elt == 'r_dt') console.log('after', val)
		return val
	}
	/* * * Headlines * * */
	var headlineList = new List('headlines', {
		item: 'headline-item',
		valueNames: [
			'h_title',
			{ name: 'h_url',	attr: 'href' },
			{ name: 'h_icon',	attr: 'src' },
			{ name: 'h_tip',	attr: 'title' },
		],
		page: HEADLINE_PAGE_SIZE,
		pagination: {
			outerWindow: 10,
			innerWindow: 10
		}
	})
	var headline_item = 1
	var headline_timeout, headline_fadeout_timeout
	function rotate_headlines_timeout() {
		headline_timeout = setTimeout(() => {
			if (headlineList.size() > HEADLINE_PAGE_SIZE) {
				headline_item = (headline_item + HEADLINE_PAGE_SIZE) % headlineList.size()
				headlineList.show(headline_item, HEADLINE_PAGE_SIZE)
				rotate_headlines()
			}
		}, 10000)
	}
	function rotate_headlines() {
		clearTimeout(headline_fadeout_timeout)
		rotate_headlines_timeout()
		fadeout_pagination_dot('#headlines', 'rgba(0, 208, 192, 1)', 1)
	}
	function fadeout_pagination_dot(id, bg_color, dot_opacity) {
		headline_fadeout_timeout = setTimeout(() => {
			var dot = document.querySelector(`${id} ul.pagination li.active a.page`)
			dot_opacity -= 0.1
			dot.style.backgroundColor = bg_color.replace(/, 1\)/, `, ${dot_opacity})`)
			fadeout_pagination_dot(id, bg_color, dot_opacity)
		}, 1000)
	}
	document.getElementById('headlines').addEventListener('mouseover', () => {
		clearTimeout(headline_timeout)
		clearTimeout(headline_fadeout_timeout)
	})
	document.getElementById('headlines').addEventListener('mouseout', async () => {
		if (await mµ.get_stored_headline_loading())
			rotate_headlines()
	})
	let set_hdl = new Set()
	document.getElementById('load_headlines').addEventListener('click', async () => {
		set_hdl.clear()
		let headline_host = mµ.get_current_source_headline_hosts(
			current_source_selection, sources_objs)
		if (! await request_permissions(headline_host)) { alert_perm() }
		load_headlines()
	})
	document.getElementById('reload_headlines').addEventListener('click', async () => {
		set_hdl.clear()
		let headline_host = mµ.get_current_source_headline_hosts(
			current_source_selection, sources_objs)
		if (! await request_permissions(headline_host)) { alert_perm() }
		load_headlines()
	})
	document.getElementById('remove_headlines').addEventListener('click', async () => {
		headlineList.clear()
		document.getElementById('headlines_wrapper').style.display = 'none'
	})
	async function load_headlines() {
		document.getElementById('headlines_wrapper').style.display = 'block'
		headlineList.clear()
		let i = 0
		for (let a of HEADLINE)
			headlineList.add({
				h_title: mp_i18n.gettext(a[0]),
				h_url: a[1],
				h_icon: "./img/favicon-metapress-v2.png",
				h_tip: mp_i18n.gettext(a[2])
			})
		headlineList.items.slice(-1)[0].values({h_icon:"./img/favicon-metapress-active.png"})
		document.querySelector('#headlines > ul >li:nth-of-type(1) a').setAttribute("target","")
		document.querySelector('#headlines > ul >li:nth-of-type(3) a').setAttribute("target","")
		let sub_load_headline = async (k) => {
			if(await mµ.check_host_perm (current_source_selection, sources_objs)) {
				document.getElementById('load_headlines').style.display = 'none'
				let n = sources_objs[k]
				if (! n.headline_url) { return }
				i += 1
				µ.sleep(1000 * i)
				let rep = await fetch(n.headline_url)
				if (!rep.ok) throw `status ${rep.status}`
				let c_type = rep.headers.get('content-type')
				if (n.tags.charset) {
					rep = await rep.arrayBuffer()
					var text_decoder = new TextDecoder(n.tags.charset, {fatal: true})
					rep = text_decoder.decode(rep)
				} else {
					rep = await rep.text()
				}
				let r = µ.dom_parser.parseFromString(rep, µ.clean_c_type(c_type))
				if(n.headline_selector && n.headline_selector != "" ) {
					try {
						if (!n.domain_part || n.extends) n.domain_part = µ.domain_part(n.headline_url)
						if (!n.favicon_url) n.favicon_url = µ.get_favicon_url(r, n.domain_part)
						let h = r.querySelector(n.headline_selector)
						let h_title = $_(!n.h_title ? 'headline_selector' : 'h_title', r, n)
						if(!set_hdl.has(h_title)) {
							set_hdl.add(h_title)
							headlineList.add({
								h_title:µ.shorten(h_title, HEADLINE_TITLE_SIZE),
								h_url:	µ.urlify(h.getAttribute('href'), n.domain_part),
								h_icon: n.favicon_url,
								h_tip: `'${n.tags.name}' (${ndt().toLocaleString(userLang)})\n`+
									`${µ.triw(h.textContent)}`
							})
						}
					} catch (exc) {console.error('header for', k, exc) }
				}
				document.getElementById('mp_headlines_page_sizes').style.display = 'block'
			}
		}
		for (let k of current_source_selection.slice(0,await mµ.get_stored_max_headline_loading()))
			sub_load_headline(k)
	}
	/* * * Tags * * */
	var select_multiple_opt = {
		removeItemButton: true,
		resetScrollPosition: false,
		placeholder: true,
		placeholderValue: '*',
		duplicateItemsAllowed: false,
		searchResultLimit: 8
	}
	var tag_select_multiple = {
		'tech': '',
		'lang': '',
		'country': '',
		'themes': '',
		'src_type': '',
		'res_type': '',
		//'scope': '',
		//freq': '',
		'name': '',
	}
	function populate_tags(source_selection) {
		var available_tags = {}
		for (let tag_name of Object.keys(tag_select_multiple)) {
			available_tags[tag_name] = {}
		}
		for (let k of source_selection) {
			let n = sources_objs[k]
			if (typeof(n) == 'undefined') return
			for (let tag_name of Object.keys(tag_select_multiple)) {
				let tag_val = n.tags[tag_name]
				if (typeof(tag_val) == 'string') {
					available_tags[tag_name][tag_val] = 1 + available_tags[tag_name][tag_val] || 1
				} else if (typeof(tag_val) == 'object') {
					for (let tag_val_itm of tag_val) {
						available_tags[tag_name][tag_val_itm] =
							1 + available_tags[tag_name][tag_val_itm] || 1
					}
				} else {
					console.warn(`Unknown tag type ${tag_name} ${tag_val} for ${k}`)
				}
			}
		}
		for (let tag_name of Object.keys(tag_select_multiple)) {
			var s = []
			var t_user_lang = ""
			for (var t of Object.keys(available_tags[tag_name]).sort()) {
				t_user_lang = mp_i18n.gettext(t)
				if(tag_name == 'lang') {
					s.push({value: t, label:
						/*`${µ.intl_lang_name(t)}<small> - <span>[${t_user_lang}]</span><span> (${*/
						`${LANG[t]}<small> - <span>[${t_user_lang}]</span><span> (${
							available_tags[tag_name][t]})</span></small>`})
				} else {
					s.push({value: t, label:
						`${t_user_lang}<small> (${available_tags[tag_name][t]})</small>`})
				}
			}
			let element = document.getElementById(`tags_${tag_name}`)
			tag_select_multiple[tag_name] = new Choices(element,
				Object.assign({choices: s}, select_multiple_opt))
		}
	}
	function dns_prefetch(current_source_selection) {
		var dns_links = document.getElementsByClassName('mp_dns_prefetch')
		for (var i = dns_links.length; i--;) {
			dns_links.item(i).remove() // parentNode.removeChild(dns_prefetch_link)
		}
		for (let i of current_source_selection) {
			let n = sources_objs[i]
			if (typeof(n) == 'undefined') return
			append_prefetch_link(n.search_url)
			if (n.redir_url)
				append_prefetch_link(n.redir_url)
		}
	}
	function append_prefetch_link(url) {
		let l = document.createElement('link')
		l.rel='dns-prefetch'
		l.href=url
		l.classList.add('mp_dns_prefetch')
		document.head.appendChild(l)
	}
	async function on_tag_sel(opt) {
		let new_src_sel = {}
		let tag_name = "name"
		let tag_name_length = tag_select_multiple[tag_name].getValue(true).length
		if (tag_name_length) {
			for (let other_mul_sel_key of Object.keys(tag_select_multiple)) {
				if (other_mul_sel_key == 'name') continue
				let other_mul_sel = tag_select_multiple[other_mul_sel_key]
				other_mul_sel.removeActiveItems()
				other_mul_sel.disable()
			}
		} else {
			for (let other_mul_sel_key of Object.keys(tag_select_multiple)) {
				if (other_mul_sel_key == 'name') continue
				let other_mul_sel = tag_select_multiple[other_mul_sel_key]
				other_mul_sel.enable()
			}
		}
		for (tag_name of Object.keys(tag_select_multiple)) {
			new_src_sel[tag_name] = {}
			for (let selected_tag of tag_select_multiple[tag_name].getValue(true)) {
				if (tag_name != 'tech') { // inner accumulation
					for (let k of sources_keys)
						if (µ.is_array(sources_objs[k].tags[tag_name])) {
							if (sources_objs[k].tags[tag_name].includes(selected_tag))
								new_src_sel[tag_name][k] = ''
						} else {
							if (sources_objs[k].tags[tag_name] === selected_tag)
								new_src_sel[tag_name][k] = ''
						}
				} else { // inner intersection
					let cur_tag_keys = []
					for (let k of sources_keys)
						if (sources_objs[k].tags[tag_name].includes(selected_tag))
							cur_tag_keys.push(k)
					if (Object.keys(new_src_sel['tech']).length == 0) {
						for (let c of cur_tag_keys)
							new_src_sel['tech'][c] = ''
						continue
					} else {
						let prec_tag_keys = Object.keys(new_src_sel[tag_name])
						new_src_sel[tag_name] = {}
						for (let k of prec_tag_keys.filter(e => cur_tag_keys.includes(e))) {
							new_src_sel[tag_name][k] = ''
						}
					}
				}
			}
			if (tag_select_multiple[tag_name].getValue(true).length > 0 &&
				Object.keys(new_src_sel[tag_name]).length == 0) {
				empty_sel(opt)
				return
			}
		}
		var new_src_sel_keys = sources_keys
		var tag_sel_keys = {}
		for (let tag_name of Object.keys(tag_select_multiple)) { // get the smallest ensemble
			if (new_src_sel_keys.length == 0) {
				break
			} else {
				tag_sel_keys = Object.keys(new_src_sel[tag_name])
				if (tag_sel_keys.length > 0)
					new_src_sel_keys = new_src_sel_keys.filter(
						v => tag_sel_keys.includes(v))
			}
		}
		if (new_src_sel_keys.length > 0) {
			current_source_selection = new_src_sel_keys
			current_source_nb = current_source_selection.length
			browser.storage.sync.set({filters: tags_toJSON()})
			if (await mµ.get_stored_live_search_reload() && document.getElementById('mp_query').value)
				document.getElementById('mp_submit').click()
			dns_prefetch(current_source_selection)
			if ((await mµ.get_stored_headline_loading() && headlineList.size() == 0)) {
				load_headlines()
			} else if(await mµ.get_stored_live_headline_reload() && headlineList.size()) {
				var h_display = document.getElementById('mp_headlines_page_sizes').style.display
				if(h_display != "none" && (opt && opt.target && opt.target.id != 'tags_tech'))
					document.getElementById('reload_headlines').click()
			}
		} else {
			empty_sel(opt)
		}
		document.getElementById('cur_tag_sel_nb').textContent = current_source_nb
	}
	function empty_sel(opt) {
		alert(mp_i18n.gettext('This choice would result in an empty selection.'))
		if (opt && typeof (opt.parentNode) != 'undefined') {
			tag_select_multiple[opt.parentNode.id.replace('tags_', '')].setValue(opt.value)
		} else {
			set_default_tags()
		}
		on_tag_sel({})
	}
	function tags_toJSON() {
		let tags = {}
		for (let t of Object.keys(tag_select_multiple))
			tags[t] = tag_select_multiple[t].getValue(true)
		return tags
	}
	function tags_toXML(type) {
		let tags = '', tag_val
		for (let t of Object.keys(tag_select_multiple)) {
			tag_val = tag_select_multiple[t].getValue(true)
			if (typeof(tag_val) == 'string') {
				if ('ATOM' == type) tags += `<category term='${tag_val}' label='mp__${t}' />\n`
				else tags += `<category>mp__${t}__${tag_val}</category>\n`
			} else if (typeof(tag_val) == 'object')
				for (let v of tag_val)
					if ('ATOM' == type) tags += `<category term='${v}' label='mp__${t}' />\n`
					else tags += `<category>mp__${t}__${v}</category>\n`
		}
		return tags
	}
	function tags_fromXML(tag_nodes, xml_type) {
		let tag_values = {}
		for (let t of Object.keys(tag_select_multiple)) tag_values[t] = []
		for (let c of tag_nodes)
			if ('ATOM' == xml_type) {
				let c_name = c.getAttribute('label')
				try {
					tag_values[c_name.split('__')[1]].push(`${c.getAttribute('term')}`)
				} catch (exc) { console.error(`Bad XML filter name '${c_name}' (${exc}).`) }
			} else {
				let c_split = c.textContent.split('__')
				try {
					tag_values[c_split[1]].push(`${c_split[2]}`)
				} catch (exc) {console.error(`Bad filter name '${c.textContent}' (${exc}).`) }
			}
		tags_fromJSON(tag_values)
		on_tag_sel({})
	}
	function tags_fromJSON(tag_values) {
		let v, s
		for (let tag_name of Object.keys(tag_values)) {
			// s = document.getElementById(`tags_${tag_name}`)
			s = tag_select_multiple[tag_name]
			v = tag_values[tag_name]
			if ('undefined' == typeof(s)) continue
			s.passedElement.element.removeEventListener('change', on_tag_sel)
			s.removeActiveItems()
			s.setChoiceByValue(v)
			s.passedElement.element.addEventListener('change', on_tag_sel)
		}
		on_tag_sel({})
	}
	function is_virgin_tags() {
		var r = true
		for (let k of Object.keys(tag_select_multiple)) {
			let v = tag_select_multiple[k].setChoiceByValue(k)
			if (v.getValue(true).length > 0) {
				r = false
			}
		}
		if(cur_querystring.get('id_sch_s')) {r=false}
		return r
	}
	function is_advanced_search() {
		var cur_tags = tags_toJSON()
		return JSON.stringify(get_default_tags()) !== JSON.stringify(cur_tags)
		/* // we used to check only country and name tags
		var r = false
		for (let k of ['country', 'name']) {
			let v = tag_select_multiple[k].setChoiceByValue(k)
			if (v.getValue(true).length > 0) {
				r = true
			}
		}
		return r*/
	}
	document.getElementById('reset_tag_sel').addEventListener('click', () => {
		set_default_tags()
	})
	function set_default_tags() {
		console.info('Meta-Press.es : loading default tags')
		tags_fromJSON(get_empty_json_tags())
		tags_fromJSON(get_default_tags())
	}
	function get_default_tags() {
		return {
			'tech': ['HTTPS', 'fast'],
			'lang': [userLang.slice(0, 2).toLowerCase()],
			'country': [],
			'themes': [],
			'src_type': ['Press'],
			'res_type': [],
			'name': [],
		}
	}
	function load_stored_tags(stored_tags) {
		if (typeof(stored_tags) == 'object' && typeof(stored_tags.filters) == 'object') {
			tags_fromJSON(stored_tags.filters)
		}
		if (is_virgin_tags())
			set_default_tags()
		if (is_advanced_search())
			document.getElementById('tags_handle').click()
		var dt_loaded_sources = new Date()
		document.getElementById('mp_loaded_source_total').textContent = sources_keys.length - 2
		document.getElementById('mp_loaded_source_duration').textContent =
			(dt_loaded_sources - dt_loading_sources) / 1000
		document.getElementById('mp_loaded_source_countries').textContent =
			Object.keys(tag_select_multiple['country']['_initialState']['choices']).length
		document.getElementById('mp_loaded_source_langs').textContent =
			Object.keys(tag_select_multiple['lang']['_initialState']['choices']).length
	}
	/* * * end tags * * */
	function update_current_sources(sources_objs) {
		sources_keys = Object.keys(sources_objs)
		current_source_selection = sources_keys
		current_source_nb = sources_keys.length
	}
	function load_source_objs(stored_data) {
		sources_objs = provided_sources
		if (typeof(stored_data) == 'object' && typeof(stored_data.custom_src) == 'string' &&
			stored_data.custom_src.length > 0) {
			try {
				let custom_src_obj = JSON.parse(stored_data.custom_src)
				sources_objs = Object.assign(provided_sources, custom_src_obj)
			} catch (exc) { alert(`Error parsing user custom source (${exc}).`) }
		}
		update_current_sources(sources_objs)
		/* * * current_source_selection defined * * */
		for (let k of sources_keys) {
			if (!k.startsWith('http')) continue  // was used to avoid false RSS and ATOM
			let n = sources_objs[k]
			if (typeof(n.extends) != 'undefined') { // Manage sources to extend
				let extended_src = sources_objs[n.extends]
				if (typeof(extended_src) != 'undefined') {
					n = mµ.extend_source(n, extended_src)
				}
			}
			let n_type = n.type
			if ("RSS" == n_type || "ATOM" == n_type) {
				n = mµ.extend_source(n, XML_SRC[n_type])
				// n.tags.tech.push('RSS')
			}
			/* if ("JSON" == n_type) {
				n.tags.tech.push('JSON')
			}*/
			let n_tech = n.tags.tech
			if (n.search_url.startsWith('https')) // Dynamically add the HTTPS src tag
				if (!n_tech.includes('HTTPS'))
					n_tech.push('HTTPS')
			let type_src = n.tags.src_type
			if (n.r_img || n.r_img_src) { // Dynamically add the Illustrated src tag
				if (!type_src.includes('Illustrated'))
					if(typeof(type_src) == 'object')
						type_src.push('Illustrated')
					else
						type_src = [type_src, 'Illustrated']
			}
			sources_objs[k] = n
			if (n.tags.tech.includes('broken')) {
				delete sources_objs[k]
				update_current_sources(sources_objs)
				continue
			}
		}
	}
	function set_query_placeholder() {
		var val = [
			'La Quadrature du Net',
			'ADPRIP',
			'Grimoire-Command.es',
			'Le Petit Prince',
			'Hong Kong',
			'Yellow vests',
			'Nuit Debout',
			'Alexandre Benalla',
			'Delta.Chat',
			'F-Droid.org',
			'OpenContacts',
			'AsciiDoctor',
			'Internet ou Minitel 2.0',
			'Pablo Servigne',
			'Laurent Gaudé',
			'Le Prince de Machiavel',
			'Pensées pour moi-même',
			'La princesse de Clèves',
			'Alpine Linux',
			'Fairphone',
			'Tor Browser',
			'Wikipedia',
			'OpenStreetMap',
			'Leto Atreides',
			'Pliocene Exile',
			'Another World by Éric Chahi',
			'5 centimeters per second',
			'Nausicaä',
			'Repair Café',
			'Reporterre',
			'Chelsea Manning',
		]
		document.getElementById('mp_query').placeholder =
			mp_i18n.gettext('Search terms, ex.') +` : ${val[µ.pick_between(0, val.length)]}`
	}
	document.getElementById('tags_handle').addEventListener('click', () => {
		document.getElementById('tags_row').style.display = document.getElementById(
			'tags_2nd_row').style.display == 'none' ? 'block' : 'none'
		document.getElementById('tags_2nd_row').style.display = document.getElementById(
			'tags_2nd_row').style.display == 'none' ? 'block' : 'none'
		document.getElementById('tags_handle').textContent=document.getElementById(
			'tags_handle').textContent == '+' ? '-' : '+'
		// cur_querystring.set('adv_search', cur_querystring.get('adv_search') ? 0 : 1)
	})
	function is_tag_in_querystring(cur_querystring) {
		for (let t of Object.keys(tag_select_multiple))
			if (cur_querystring.get(t) || cur_querystring.get('id_sch_s'))
				return true
		return false
	}
	function get_empty_json_tags() {
		var json_obj = {}
		for (let t of Object.keys(tag_select_multiple))
			json_obj[t] = []
		return json_obj
	}
	function querystring_to_json(sp) {
		var json_obj = get_empty_json_tags()
		for (const [key, value] of sp) {
			if (typeof(json_obj[key]) != 'undefined') {
				json_obj[key].push(value)
			}
		}
		return json_obj
	}
	function manage_mp_input_focus() {
		// for (let y of document.querySelectorAll('.choices__input'))
		for (let y of document.querySelectorAll(
			'.choices__list--dropdown > .choices__list > .choices__item')) {
			y.addEventListener('click', () => { // is not called
				document.querySelector('#mp_query').focus()
			}, true)
		}
	}
	var id_elt, last_elt, browser_str
	async function add_elt_url(id) {
		id_elt = id.split('__')[1]
		var elt = document.getElementById('result_list').firstChild.querySelector('time').dateTime
		last_elt = new Date(elt).toUTCString()
		await browser.storage.sync.get().then(setElt)
	}
	async function setElt(elt) {
		browser_str = elt
		await getting.then((a)=>{return a.init_table_sch_s(browser_str)})
		var table_sch_s = elt.sch_sea
		let local_url = gen_permalink(false)
		let new_url = new URL(local_url.origin + local_url.pathname + table_sch_s[id_elt])
		let sto_r = new_url.searchParams.get('last_res')
		let temp= await mµ.set_text_params(new_url)
		var list_p = temp['list_p']
		var params = temp['params']
		var body_txt = `${mp_i18n.gettext(' results for "')}`+list_p['q']+'"\n' + params
		local_url.searchParams.delete('last_res')
		local_url.searchParams.sort()
		new_url.searchParams.delete('last_res')
		new_url.searchParams.sort()
		if(local_url.search==new_url.search) {
			//si local.search == new_url.search on lance la suite sinon non
			sto_r = sto_r ? sto_r : 0
			new_url.searchParams.set('last_res', last_elt)
			table_sch_s[id_elt] = new_url.search
			var nb_new_res = await count_new_res(sto_r)
			if(nb_new_res > 0) {
				document.getElementById('favicon').href="img/favicon-metapress-active.png"
				if (nb_new_res >= 10) { nb_new_res = "+" + nb_new_res }
				body_txt = nb_new_res + mp_i18n.gettext(' new ') + body_txt
				var notif_txt = mp_i18n.gettext('New result for schedule search')
				mµ.notifyUser(`${notif_txt} ${id_elt}`, body_txt)
			}
			for(let i; i<table_sch_s.length;i++) {
				var m_id_sch_s = `sch_sea__${i}`
				var m_value_sch_s = table_sch_s[i]
				await getting.then((elt)=>{elt.modif_sch_s_storage(m_id_sch_s, m_value_sch_s)})
			}
		}
	}
	function count_new_res(sto_r) {
		var results = document.getElementById('result_list').children
		var count = 0
		for(let r of results) {
			var dt_r = r.querySelector('time').dateTime
			if(new Date(sto_r) < new Date(dt_r)) {count+=1}
			else break
		}
		return count
	}
	const XML_SRC = {
		'RSS': {
			'filters': 'category',
			'results': 'item',
			'r_h1': 'title',
			'r_url': 'link',
			'r_dt': 'pubDate',
			'r_txt': 'description',
			'r_by_xpath': './dc:creator',
			'docs': 'https://www.w3.org/TR/selectors-3/#attrnmsp',
			'tags': {'name': 'RSS','lang':[],'country':'en','themes':[],'tech':[],'freq':[],
				'scope':[],'src_type':[],'res_type':[]}
		},
		'ATOM': {
			'filters': 'category',
			'results': 'entry',
			'r_h1': 'title',
			'r_url': 'link',
			'r_url_attr': 'href',
			'r_dt': 'published',
			'r_txt': 'summary',
			'r_by': 'author',
			'tags': {'name': 'ATOM','lang':[],'country':'en','themes':[],'tech':[],'freq':[],
				'scope':[],'src_type':[],'res_type':[]}
		}
	}
	const HEADLINE = []
	var mp_hl = document.querySelectorAll('.mp_hl')
	for (let h of mp_hl) {
		let v = h.innerHTML
		let u = h.href
		let t = h.getAttribute('title')
		HEADLINE.push([v,u,t])
	}
	/* * end definitions * */
	await mµ.set_theme()
	var dt_loading_sources = new Date()
	var getting = browser.runtime.getBackgroundPage()
	let month_nb_json = await fetch('js/month_nb/month_nb.json')
	month_nb_json = await month_nb_json.json()
	const cur_querystring = new URL(window.location).searchParams
	const img_load = 'photo_loading_placeholder.png'
	const test_src = cur_querystring.get('test_sources')
	if (test_src == '1') {
		var src_name = cur_querystring.get('name')
		provided_sources = await getting.then((a) => {return a.get_prv_src(src_name)})
		current_source_selection = await Object.keys(provided_sources)
		load_source_objs()
		document.getElementById('mp_submit').click()
	} else {
		provided_sources = await getting.then((a) => {return a.get_prv_src("")})
		load_source_objs(await browser.storage.sync.get('custom_src'))
		let is_launch_test = cur_querystring.get('launch_test')
		let sch_search_test = cur_querystring.get('submit')
		if(is_launch_test) {
			if(sch_search_test==1) {
				test_sources(provided_sources)
				results_test(true)
			} else {
				let a = document.getElementById('mp_start_test')
				a.style = 'display:inline-block;'
				a.addEventListener('click', async() => {
					a.style = 'display:none;'
					let s_surl = mµ.get_current_source_search_hosts(
						Object.keys(provided_sources), provided_sources)
					if(await request_permissions(s_surl)) {
						test_sources(provided_sources)
						results_test(true)
					}
				})
			}
		}
		let lang_names = await fetch('json/lang_names.json')
		var mp_i18n = await g.gettext_html_auto(userLang) // must be before populate tags
		var LANG = await lang_names.json() // must be before populate tags
		populate_tags(current_source_selection)
		var alt_load_txt = mp_i18n.gettext('Click to load image')
		await g.xgettext_html()
		var do_manage_mp_input_focus = false
		if (do_manage_mp_input_focus)
			manage_mp_input_focus() // must be after populate_tags
		// removed to allow kbd nav downward
		µ.set_html_lang(userLang)
		if(cur_querystring.get('id_sch_s') && !cur_querystring.get('submit')) {
			let modif_txt = mp_i18n.gettext('save modification')
			document.querySelector('.add_auto_s').innerHTML =
				`<span class="black">&#9999;&#65039;</span> ${modif_txt}`
		}
		load_stored_tags(is_tag_in_querystring(cur_querystring) ?
			{filters: querystring_to_json(cur_querystring)} :
			await browser.storage.sync.get('filters')
		)
		dns_prefetch(current_source_selection)
		set_query_placeholder()
		let manifest = await fetch('manifest.json')
		manifest = await manifest.json()
		document.getElementById('mp_version').textContent = 'v' + manifest.version
		rotate_headlines_timeout()
		if (cur_querystring.get('submit')) {
			document.getElementById('mp_submit').click()
		}
		if(test_src==0) {
			let elt = document.getElementById('refresh_src')
			elt.style = 'display:inline-block'
			elt.addEventListener('click', async() => {
				await getting.then((e) => {e.load_prov_src()})
				reload_keeping_query()
			})
		}
	}
})()

